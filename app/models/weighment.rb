class Weighment < ActiveRecord::Base
  attr_accessible :amc_id, :bag_type, :is_active, :lot_code, :lot_id, :lot_weight, :lot_weight_in_no, :no_of_bags, :product_id, :remarks, :seller_name, :seller_type, :weighment_charges, :weighment_type
end
