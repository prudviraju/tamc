class VehicleType < ActiveRecord::Base
  attr_accessible :amc_id, :description, :is_active, :name
end
