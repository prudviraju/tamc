class GateEntry < ActiveRecord::Base
  attr_accessible :amc_id, :gatepass_no,:vechile_no, :vehicle_type_id,:created_by,:gate_entry_type,:device_id,:deviceType,:tempLot,:trade_type,:posted_on,:cold_storage_id

  def self.posted_date_scope(posted_on)
    if posted_on.blank?
      scoped
    else
      where("posted_on=?", posted_on)
    end
  end

  def self.gate_pass_scope(gate_pass)
    if gate_pass.blank?
      scoped
    else
      where("gatepass_no =?", gate_pass)
    end
  end

  def self.vechile_no_scope(vechile_no)
    if vechile_no.blank?
      scoped
    else
      where("vechile_no =?", vechile_no)
    end
  end

end
