class Bid < ActiveRecord::Base
  attr_accessible :agent_id, :bid_amount, :is_active, :is_approved, :lot_id, :seller_id
end
