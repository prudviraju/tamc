class Lot < ActiveRecord::Base
  attr_accessible :amc_id, :aprox_quantity, :bag_type_id, :ca_firm, :ca_name, :commodity_id, :district, :gate_entry_pass_no, :is_bid_rigistred, :is_gate_entry, :is_takpatti, :is_weighment, :lot_code, :mandal, :mobile_no, :no_of_bags, :seller_id, :seller_type, :state, :vechile_no, :vehicle_type_id, :village, :bid_amount, :bid_raise_by, :posted_on,:farmer_amount,:trader_payable_amount,:market_charges,:gunny_amount,:coldstore_id,:remarks


  def self.seller_scope(seller_id)
    if seller_id.blank?
      scoped
    else
      where("seller_id=?", seller_id)
    end
  end

  def self.trader_scope(ta_id)
    if ta_id.blank?
      scoped
    else
      where("bid_raise_by=?", ta_id)
    end
  end

  def self.posted_scope(posted_on)
    if posted_on.blank?
      scoped
    else
      where("posted_on=?", posted_on)
    end
  end

  def self.posted_scope1(from_date,to_date)
    if from_date.blank? && to_date.blank?
      scoped
    else
      where("posted_on >= ? and posted_on <= ?", from_date,to_date)
    end
  end

  def self.bid_scope(from,to)
    if from.blank? || to.blank?
      scoped
    else
      where("bid_amount >= ? and bid_amount <= ?", from,to)
    end
  end


  def self.lot_code_scope(lot_code)
    if lot_code.blank?
      scoped
    else
      where("lot_code=?", lot_code)
    end
  end


  def self.commodiy_scope(commodity_id)
    if commodity_id.blank?
      scoped
    else
      where("commodity_id=?", commodity_id)
    end
  end


  def self.ca_scope(ca_id)
    if ca_id.blank?
      scoped
    else
      where(" ca_name =?", ca_id)
    end
  end

  def self.trader_scope(bid_id)
    if bid_id.blank?
      scoped
    else
      where(" bid_raise_by =?", bid_id)
    end
  end


  def self.cold_scope(coldstore_id)
    if coldstore_id.blank?
      scoped
    else
      where(" coldstore_id =?", coldstore_id)
    end
  end



end
