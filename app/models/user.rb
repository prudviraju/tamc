class User < ActiveRecord::Base
  acts_as_authentic
  attr_accessible :aadhar_no, :amc_id, :crypted_password, :district, :door_no, :email, :is_active, :is_admin, :is_super_admin, :mandal, :mobile_no, :name, :password, :password1, :password_confirmation, :password_salt, :perishable_token, :persistence_token, :pincode, :user_role, :village,:is_employee,:user_code,:segment_id


  def role_symbols
    result = [:guest]
    result << :super_admin if is_super_admin
    result << :admin if is_admin
    result << :GateEntryOperator if user_role.eql?('GateEntryOperator')
    result << :Trader if user_role.eql?('Trader') || user_role.eql?('CommissionAgent')
    result
  end


  def self.name_scope(name)
    if name.blank?
      scoped
    else
      where(" name like ?","%#{name}%")
    end
  end


  def self.mobile_number_scope(mobile_number)
    if mobile_number.blank?
      scoped
    else
      where("mobile_no=?", mobile_number)
    end
  end

  def self.company_name_scope(company_name)
    if company_name.blank?
      scoped
    else
      where("company_name like ?", "%#{company_name}%")
    end
  end


  def self.amc_scope(amc_id)
    if amc_id.blank?
      scoped
    else
      where("amc_id=?", amc_id)
    end
  end


  def self.admin_scope()
      where("is_admin=?", false)
  end

  def self.employee_scope()
      where("is_employee=?", false)
  end

  def self.getAllCaList(prefix, amc)
    ca_names = where("(name like ? and user_role=? and amc_id= ?)", "%#{prefix}%",'CommissionAgent', amc).order('name ASC')
  end


  # def is_superadmin
  #   self.is_super_admin.present? && self.is_super_admin
  # end
  #
  # def is_admin
  #   self.is_admin.present? && self.is_admin
  # end

end
