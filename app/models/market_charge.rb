class MarketCharge < ActiveRecord::Base
  attr_accessible :above_weight, :amc_id, :attachement, :below_weight, :charge_type, :charge_value, :commodity_id, :deduction_type, :is_active, :modified_by, :modified_on, :name, :party_type, :unit
end
