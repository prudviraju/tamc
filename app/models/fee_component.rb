class FeeComponent < ActiveRecord::Base
  attr_accessible :amount, :amount_per_uom, :calculate_on_uom, :is_active, :modified_by, :modified_on, :name, :party_type, :percentage, :tds, :fee_type,:amc_id,:fee_category_id
end
