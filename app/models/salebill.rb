class Salebill < ActiveRecord::Base
  attr_accessible :actual_weight, :agreement_type, :amc_id, :apx_weight, :bag_type_id, :commodity_id, :fee_category_id, :firm_name, :gross_amount, :lot_code, :lot_id, :net_amount, :net_payable, :net_weight, :no_of_bags, :rate_for_qui, :seller_id, :seller_type, :trader_id
end
