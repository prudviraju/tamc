class Farmer < ActiveRecord::Base
  attr_accessible :account_number, :amc_id, :bank_name, :category, :contact_no, :date_of_birth, :district, :email, :gender, :id_Proff_type, :ifsc_code, :is_active, :locality, :mandal, :name, :pin_code, :proof_id, :registration_date, :registration_level, :relation, :relative_name, :remarks, :service_provider, :state, :village ,:old_farmer


  def self.name_scope(name)
    if name.blank?
      scoped
    else
      where("name like? ", '%'+name+'%')
    end
  end

  def self.mobile_no_scope(mobile_no)
    if mobile_no.blank?
      scoped
    else
      where("contact_no like ?", '%'+mobile_no+'%')
    end
  end

  def self.village_scope(village)
    if village.blank?
      scoped
    else
      where("village =?", village)
    end
  end

  def self.amcscope(amc_id)
    if amc_id.blank?
      scoped
    else
      where("amc_id =?", amc_id)
    end
  end

  def self.get_farmers(prefix, amc)
    vendor_names = where("(name like ? and amc_id= ? and old_farmer =?)", "%#{prefix}%", amc,false).order('name ASC')
  end
end
