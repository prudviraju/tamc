class SubLot < ActiveRecord::Base
  attr_accessible :amc_id, :bid_amount, :device_id, :grossWeight, :gross_amount, :is_active, :lot_id, :netWeight, :net_amount, :net_payable, :no_of_bags, :tareWeight, :user_id
end
