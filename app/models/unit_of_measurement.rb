class UnitOfMeasurement < ActiveRecord::Base
  attr_accessible :amc_id, :description, :is_active, :unit_name
end
