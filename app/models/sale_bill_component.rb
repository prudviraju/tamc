class SaleBillComponent < ActiveRecord::Base
  attr_accessible :amount, :amount_calculated_type, :component_id, :deducted_amount, :discount_percent, :party_id, :party_name, :party_type, :percent, :salebill_id
end
