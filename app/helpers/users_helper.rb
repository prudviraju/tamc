module UsersHelper


  class UserInformation
    attr_accessor :oprId, :orgId, :userId,:userName,:loginType,:refVal,:userType, :createdInOpr, :statusMsg,:stateId,:stateName,:contactNo,:emailId,:userNameEn,:oprName
    def initialize(oprId, orgId, userId,userName,loginType,refVal,userType, createdInOpr, statusMsg,stateId,stateName,contactNo,emailId,userNameEn,oprName)
      @oprId = oprId
      @orgId= orgId
      @userId = userId
      @userName = userName
      @loginType = loginType
      @refVal = refVal
      @userType = userType
      @createdInOpr= createdInOpr
      @statusMsg =statusMsg
      @stateId =stateId
      @stateName= stateName
      @contactNo =contactNo
      @emailId = emailId
      @userNameEn =userNameEn
      @oprName =oprName
    end
  end


  class FinalUserInformation
    attr_accessor :statusMsg, :listLogin
    def initialize(statusMsg, listLogin)
      @statusMsg = statusMsg
      @listLogin= listLogin
    end
  end

  class AgentInformation
    attr_accessor :caId, :caName,:caMobileNo, :caFirmName,:agentRole,:licenceNo,:panCardNo,:trnNo,:statusMsg,:failMsg
    def initialize(caId, caName,caMobileNo, caFirmName,agentRole,licenceNo,panCardNo,trnNo,statusMsg,failMsg)
      @caId = caId
      @caName= caName
      @caMobileNo =caMobileNo
      @caFirmName = caFirmName
      @agentRole = agentRole
      @licenceNo = licenceNo
      @panCardNo = panCardNo
      @trnNo = trnNo
      @statusMsg = statusMsg
      @failMsg = failMsg
    end
  end

  class FinalAgentInformation
    attr_accessor :statusMsg, :gateEntryCommissionAgentList
    def initialize(statusMsg, gateEntryCommissionAgentList)
      @statusMsg = statusMsg
      @gateEntryCommissionAgentList= gateEntryCommissionAgentList
    end
  end


  class HwInformation
    attr_accessor :agentId, :agentName,:agentType
    def initialize(agentId, agentName,agentType)
      @agentId = agentId
      @agentName= agentName
      @agentType =agentType
    end
  end

  class FinalHwInformation
    attr_accessor :statusMsg, :listLotFeeMapAgentModel
    def initialize(statusMsg, listLotFeeMapAgentModel)
      @statusMsg = statusMsg
      @listLotFeeMapAgentModel= listLotFeeMapAgentModel
    end
  end

end
