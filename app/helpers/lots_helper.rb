module LotsHelper

  class LotResponse
    attr_accessor :statusMsg, :lotId,:lotCode,:commodityId,:commodityName,:noOfBags,:typeOfBag,:commodityType,:vehicleNo,:vehicleType,:vehicleDesc,:farmerId,:farmerName,:caId,:caName,:traderId,:traderName,:lotRate,:emptyBagWeight,:getListofLotDtl,:marketFee,:commission,:gunnyCost
    def initialize(statusMsg, lotId,lotCode,commodityId,commodityName,noOfBags,typeOfBag,commodityType,vehicleNo,vehicleType,vehicleDesc,farmerId,farmerName,caId,caName,traderId,traderName,lotRate,emptyBagWeight,getListofLotDtl,marketFee,commission,gunnyCost)
      @statusMsg = statusMsg
      @lotId= lotId
      @lotCode= lotCode
      @commodityId =commodityId
      @commodityName = commodityName
      @noOfBags = noOfBags
      @typeOfBag = typeOfBag
      @commodityType = commodityType
      @vehicleNo = vehicleNo
      @vehicleType = vehicleType
      @vehicleDesc = vehicleDesc
      @farmerId = farmerId
      @farmerName = farmerName
      @caId = caId
      @caName = caName
      @traderId = traderId
      @traderName = traderName
      @lotRate = lotRate
      @emptyBagWeight = emptyBagWeight
      @getListofLotDtl =getListofLotDtl
      @marketFee = marketFee
      @commission = commission
      @gunnyCost = gunnyCost
    end
  end

  class AllLotInformation
  attr_accessor :orgId, :userId,:loginType,:statusMsg,:listLogin,:getListofLotDtl
  def initialize(orgId, userId,loginType,statusMsg,listLogin,getListofLotDtl)
    @orgId = orgId
    @userId= userId
    @loginType= loginType
    @statusMsg =statusMsg
    @listLogin = listLogin
    @getListofLotDtl = getListofLotDtl
  end
end

  class LotNumberResponse
    attr_accessor :lotId
    def initialize(lotId )
      @lotId = lotId
    end
  end

end
