module GateEntriesHelper


  class FinalResponse
    attr_accessor :statusMsg, :failMsg,:listLots,:receiptNo
    def initialize(statusMsg, failMsg,listLots,receiptNo)
      @statusMsg = statusMsg
      @failMsg= failMsg
      @listLots= listLots
      @receiptNo =receiptNo
    end
  end
end
