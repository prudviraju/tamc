module ApplicationHelper
  CAMMA = ', '
  def get_index_text(value)
    "<div class=\"row\">
  <div class=\"col-sm-12\" style=\"padding-left: 0px;\">
  <div class=\"head_style\">" "#{value}</div></div></div>".html_safe
  end

  def self.get_root_url
    "http://13.232.229.101/"
  end


  module SessionConstants
    LAST_CREATED_FARMER = :last_created_farmer
    SELECTED_ID = :selected_id
  end

  def series_prefix(input)
    "{name: '#{input}', data: ["
  end

  def get_plot_time_format(date)
    result=''
    if !date.blank?
      result = "Date.UTC(#{date.to_s.gsub('-', ', ')})"
    end
    result
  end


  def get_formatted_plot_data(input)
    replace_delimitor(input, ApplicationHelper::CAMMA)+"]},"
  end

  def replace_delimitor(data, delimitor)
    result=''
    if data.ends_with?(delimitor)
      result = data[0, data.length-delimitor.length]
    end
    result
  end


  def has_admin
    current_user.present? &&current_user.is_admin
  end

  def has_super_admin
    current_user.present? &&current_user.is_super_admin
  end

end
