module FarmersHelper



  class CommodityInformation
    attr_accessor :prodId, :prodName, :bagTypeId,:commUomId,:trnNo,:unitCode,:statusMsg, :failMsg
    def initialize(prodId, prodName, bagTypeId,commUomId,trnNo,unitCode,statusMsg, failMsg)
      @prodId = prodId
      @prodName= prodName
      @bagTypeId = bagTypeId
      @commUomId = commUomId
      @trnNo = trnNo
      @unitCode = unitCode
      @statusMsg = statusMsg
      @failMsg= failMsg
    end
  end

  class FinalCommodityInformation
    attr_accessor :statusMsg, :gateEntryCommodityList
    def initialize(statusMsg, gateEntryCommodityList)
      @statusMsg = statusMsg
      @gateEntryCommodityList= gateEntryCommodityList
    end
  end

  class BagTypesInformation
    attr_accessor :statusMsg, :bagTypeId, :bagTypeDesc,:bagQuantity,:bagUomId,:failMsg
    def initialize(statusMsg, bagTypeId, bagTypeDesc,bagQuantity,bagUomId,failMsg)
      @statusMsg = statusMsg
      @bagTypeId = bagTypeId
      @bagTypeDesc = bagTypeDesc
      @bagQuantity = bagQuantity
      @bagUomId= bagUomId
      @failMsg= failMsg
    end
  end

  class FinalBagTypesInformation
    attr_accessor :statusMsg, :listBagType
    def initialize(statusMsg, listBagType)
      @statusMsg = statusMsg
      @listBagType= listBagType
    end
  end

  class VechileTypesInformation
    attr_accessor :statusMsg, :vehicleTypeId, :vehicleTypeDesc
    def initialize(statusMsg, vehicleTypeId, vehicleTypeDesc)
      @statusMsg = statusMsg
      @vehicleTypeId = vehicleTypeId
      @vehicleTypeDesc = vehicleTypeDesc
    end
  end



  class FinalVechileTypesInformation
    attr_accessor :statusMsg, :listVehicleType
    def initialize(statusMsg, listVehicleType)
      @statusMsg = statusMsg
      @listVehicleType= listVehicleType
    end
  end


  class FarmerInformation
    attr_accessor :sellerId, :sellerName, :mobileNo, :state, :district, :tahsil, :city, :aadharCardNo, :idProof, :companyName, :licenceNo, :trnNo, :statusMsg, :failMsg
    def initialize(sellerId, sellerName, mobileNo, state, district, tahsil, city, aadharCardNo, idProof, companyName, licenceNo, trnNo, statusMsg, failMsg)
      @sellerId = sellerId
      @sellerName = sellerName
      @mobileNo = mobileNo
      @state = state
      @district = district
      @tahsil = tahsil
      @city = city
      @aadharCardNo = aadharCardNo
      @idProof = idProof
      @companyName = companyName
      @licenceNo = licenceNo
      @trnNo = trnNo
      @statusMsg = statusMsg
      @failMsg = failMsg
    end
  end

  class FinalFarmerInformation
    attr_accessor :statusMsg, :sellerList

    def initialize(statusMsg, sellerList)
      @statusMsg = statusMsg
      @sellerList = sellerList
    end
  end





end
