module FeeCategoriesHelper


  class FeeCategoriesInformation
    attr_accessor :orgId, :oprId,:feeCategoryId,:feeCategoryName
    def initialize(orgId, oprId,feeCategoryId,feeCategoryName)
      @orgId = orgId
      @oprId= oprId
      @feeCategoryId = feeCategoryId
      @feeCategoryName =feeCategoryName
    end
  end

  class FinalFeeCategoriesInformation
    attr_accessor :statusMsg, :listFeeCategoryForAutoSaleModel
    def initialize(statusMsg, listFeeCategoryForAutoSaleModel)
      @statusMsg = statusMsg
      @listFeeCategoryForAutoSaleModel= listFeeCategoryForAutoSaleModel
    end
  end


  class FeeComponentInformation
    attr_accessor :fcmComponentId, :fcmComponentDesc, :fcmIncludExclud, :fcdPartyType

    def initialize(fcmComponentId, fcmComponentDesc, fcmIncludExclud, fcdPartyType)
      @fcmComponentId = fcmComponentId
      @fcmComponentDesc = fcmComponentDesc
      @fcmIncludExclud = fcmIncludExclud
      @fcdPartyType = fcdPartyType
    end
  end

  class FinalFeeComponentsInformation
    attr_accessor :statusMsg, :listFeeComponentMstDtlAutoSaleModel

    def initialize(statusMsg, listFeeComponentMstDtlAutoSaleModel)
      @statusMsg = statusMsg
      @listFeeComponentMstDtlAutoSaleModel = listFeeComponentMstDtlAutoSaleModel
    end
  end


end
