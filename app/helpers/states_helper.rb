module StatesHelper

  class StatesInformation
    attr_accessor :statusMsg, :stateId, :stateName

    def initialize(statusMsg, stateId, stateName)
      @statusMsg = statusMsg
      @stateId = stateId
      @stateName = stateName
    end
  end
  class FinalStatesInformation
    attr_accessor :statusMsg, :stateList

    def initialize(statusMsg, stateList)
      @statusMsg = statusMsg
      @stateList = stateList
    end
  end

  class DistrictInformation
    attr_accessor :statusMsg, :stateId, :districtId, :districtName

    def initialize(statusMsg, stateId, districtId, districtName)
      @statusMsg = statusMsg
      @stateId = stateId
      @districtId = districtId
      @districtName = districtName
    end
  end


  class FinalDistrictInformation
    attr_accessor :statusMsg, :districtList

    def initialize(statusMsg, districtList)
      @statusMsg = statusMsg
      @districtList = districtList
    end
  end


  class MandInformation
    attr_accessor :statusMsg, :stateId, :districtId, :tahsilId, :tahsilName

    def initialize(statusMsg, stateId, districtId, tahsilId, tahsilName)
      @statusMsg = statusMsg
      @stateId = stateId
      @districtId = districtId
      @tahsilId = tahsilId
      @tahsilName = tahsilName
    end
  end


  class FinalmandalInformation
    attr_accessor :statusMsg, :tahsilList

    def initialize(statusMsg, tahsilList)
      @statusMsg = statusMsg
      @tahsilList = tahsilList
    end
  end

  class CityInformation
    attr_accessor :statusMsg, :stateId, :districtId, :tahsilId, :cityId,:cityName

    def initialize(statusMsg, stateId, districtId, tahsilId, cityId,cityName)
      @statusMsg = statusMsg
      @stateId = stateId
      @districtId = districtId
      @tahsilId = tahsilId
      @cityId = cityId
      @cityName = cityName
    end
  end

  class FinalCityInformation
    attr_accessor :statusMsg, :cityList

    def initialize(statusMsg, cityList)
      @statusMsg = statusMsg
      @cityList = cityList
    end
  end

end
