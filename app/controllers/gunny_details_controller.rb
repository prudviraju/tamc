class GunnyDetailsController < ApplicationController
  # GET /gunny_details
  # GET /gunny_details.json
  def index



    if current_user.present? && current_user.amc_id.present?
      @gunny_details = GunnyDetail.where("amc_id =?",current_user.amc_id)
    else
      @gunny_details = GunnyDetail.all
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @gunny_details }
    end
  end

  # GET /gunny_details/1
  # GET /gunny_details/1.json
  def show
    @gunny_detail = GunnyDetail.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gunny_detail }
    end
  end

  # GET /gunny_details/new
  # GET /gunny_details/new.json
  def new
    @gunny_detail = GunnyDetail.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @gunny_detail }
    end
  end

  # GET /gunny_details/1/edit
  def edit
    @gunny_detail = GunnyDetail.find(params[:id])
  end

  # POST /gunny_details
  # POST /gunny_details.json
  def create
    @gunny_detail = GunnyDetail.new(params[:gunny_detail])

    respond_to do |format|
      if @gunny_detail.save
        format.html { redirect_to '/gunny_details', notice: 'Gunny detail was successfully created.' }
        format.json { render json: @gunny_detail, status: :created, location: @gunny_detail }
      else
        format.html { render action: "new" }
        format.json { render json: @gunny_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /gunny_details/1
  # PUT /gunny_details/1.json
  def update
    @gunny_detail = GunnyDetail.find(params[:id])

    respond_to do |format|
      if @gunny_detail.update_attributes(params[:gunny_detail])
        format.html { redirect_to '/gunny_details', notice: 'Gunny detail was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gunny_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gunny_details/1
  # DELETE /gunny_details/1.json
  def destroy
    @gunny_detail = GunnyDetail.find(params[:id])
    @gunny_detail.destroy

    respond_to do |format|
      format.html { redirect_to gunny_details_url }
      format.json { head :no_content }
    end
  end
end
