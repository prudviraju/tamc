class AmcsController < ApplicationController
  # GET /amcs
  # GET /amcs.json
  filter_resource_access
  filter_access_to :all_amc_list, :attribute_check => false
  filter_access_to :select_amc, :attribute_check => false
  def index
    @amcs = Amc.all

    respond_to do |format|
      format.html # index.html.erb
      format.json {render json: @amcs}
    end
  end

  # GET /amcs/1
  # GET /amcs/1.json
  def show
    @amc = Amc.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json {render json: @amc}
    end
  end

  # GET /amcs/new
  # GET /amcs/new.json
  def new
    @amc = Amc.new

    respond_to do |format|
      format.html # new.html.erb
      format.json {render json: @amc}
    end
  end

  # GET /amcs/1/edit
  def edit
    @amc = Amc.find(params[:id])
  end

  # POST /amcs
  # POST /amcs.json
  def create
    @amc = Amc.new(params[:amc])

    respond_to do |format|
      if @amc.save
        format.html {redirect_to '/amcs', notice: 'Amc was successfully created.'}
        format.json {render json: @amc, status: :created, location: @amc}
      else
        format.html {render action: "new"}
        format.json {render json: @amc.errors, status: :unprocessable_entity}
      end
    end
  end

  # PUT /amcs/1
  # PUT /amcs/1.json
  def update
    @amc = Amc.find(params[:id])

    respond_to do |format|
      if @amc.update_attributes(params[:amc])
        format.html {redirect_to '/amcs', notice: 'Amc was successfully updated.'}
        format.json {head :no_content}
      else
        format.html {render action: "edit"}
        format.json {render json: @amc.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /amcs/1
  # DELETE /amcs/1.json
  def destroy
    @amc = Amc.find(params[:id])
    @amc.destroy

    respond_to do |format|
      format.html {redirect_to amcs_url}
      format.json {head :no_content}
    end
  end

  def all_amc_list
    @amcs = Amc.all
  end


  def select_amc
    @current_user.update_attribute(:amc_id, params[:amc_id]) if params[:amc_id].present? && current_user.present? && current_user.is_super_admin

    redirect_to "/farmers/dashboard"
  end

end
