class GateEntriesController < ApplicationController
  # GET /gate_entries
  # GET /gate_entries.json
  include GateEntriesHelper
  include ApplicationHelper

  def index
    @limit = !params[:limit].blank? ? params[:limit] : 15
    @offset = !params[:offset].blank? ? params[:offset] : 0

    @farmer = []
    if current_user.present? && current_user.amc_id.present?

      if params[:posted_on].blank? && params[:posted_on].blank? && params[:posted_on].blank?
        @gate_count = GateEntry.where("amc_id=?", current_user.amc_id).posted_date_scope(Date.today).size()
        @gate_entries = GateEntry.where("amc_id=?", current_user.amc_id).posted_date_scope(Date.today).limit(@limit).offset(@offset).reverse
      else
        @gate_count = GateEntry.where("amc_id=?", current_user.amc_id).posted_date_scope(params[:posted_on]).gate_pass_scope(params[:gate_pass]).vechile_no_scope(params[:vechile_no]).size()
        @gate_entries = GateEntry.where("amc_id=?", current_user.amc_id).posted_date_scope(params[:posted_on]).gate_pass_scope(params[:gate_pass]).vechile_no_scope(params[:vechile_no]).limit(@limit).offset(@offset).reverse
      end

    else
      @gate_count = GateEntry.posted_date_scope(params[:posted_on]).gate_pass_scope(params[:gate_pass]).vechile_no_scope(params[:vechile_no]).size()
      @gate_entries = GateEntry.posted_date_scope(params[:posted_on]).gate_pass_scope(params[:gate_pass]).vechile_no_scope(params[:vechile_no]).limit(@limit).offset(@offset).reverse
    end
    respond_to do |format|
      format.html # index.html.erb
      format.js
      format.json {render json: @gate_entries}
    end
  end

  # GET /gate_entries/1
  # GET /gate_entries/1.json
  def show
    @gate_entry = GateEntry.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gate_entry }
    end
  end

  # GET /gate_entries/new
  # GET /gate_entries/new.json
  def new
    @gate_entry = GateEntry.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @gate_entry }
    end
  end

  # GET /gate_entries/1/edit
  def edit
    @gate_entry = GateEntry.find(params[:id])
  end

  # POST /gate_entries
  # POST /gate_entries.json
  def create
    @gate_entry = GateEntry.new(params[:gate_entry])
    @gate_entry.gatepass_no = "G"

    @gate_entry.vehicle_type_id = 1 if @gate_entry.vehicle_type_id.blank?
    @gate_entry.vechile_no = "VEHICLE"  if @gate_entry.vechile_no.blank?

    respond_to do |format|
      if @gate_entry.save
        @gate_entry.created_by = current_user.id
        @gate_entry.deviceType = "SERVER"
        @gate_entry.device_id = "SERVER"
        @gate_entry.entry_time = Time.now
        @gate_entry.posted_on = Date.today
        @gate_entry.gatepass_no = @gate_entry.gatepass_no.to_s + '-' + @gate_entry.id.to_s
        @gate_entry.save
        params.keys.each do |key|
          if key.starts_with?('sellerId_')
            split_keys = key.split('_')
            index = key.split('_')[1]
            if params["sellerId_#{index}"].present? && params["commodityId_#{index}"].present? && params["noOfBags_#{index}"].present?

              @farmer = Farmer.new
              @farmer.name = params["sellerId_#{index}"]
              @farmer.state = "TELANGANA"
              @farmer.district = params["district_#{index}"]
              @farmer.mandal = params["mandal_#{index}"]
              @farmer.village = params["village_#{index}"]
              @farmer.contact_no = params["mobileNo_#{index}"]
              @farmer.amc_id = current_user.amc_id
              @farmer.old_farmer = false
              @farmer.save

            @lot = Lot.new
            @lot.seller_type = 'FARMER'
            @lot.seller_id =  @farmer.id
            @lot.state = @farmer.state
            @lot.mandal = @farmer.mandal
            @lot.village = @farmer.village
            @lot.district = @farmer.district
            @lot.mobile_no =  params["mobileNo_#{index}"]
            @lot.commodity_id =  params["commodityId_#{index}"].to_i
            @commodity = Commodity.find_by_id(params["commodityId_#{index}"].to_i)
            @lot.bag_type_id = @commodity.bag_type_id  if @commodity.present? && @commodity.bag_type_id.present?
            @lot.no_of_bags =  params["noOfBags_#{index}"].to_i
              #@ca = User.find_by_id(params["caName_#{index}"].split('-')[1])
            @lot.ca_name =  params["caName_#{index}"].to_i
            @lot.ca_firm =  User.find_by_id(params["caName_#{index}"].to_i).try(:company_name)
            @lot.vehicle_type_id =  @gate_entry.vehicle_type_id
            @lot.vechile_no =  @gate_entry.vechile_no
            @lot.aprox_quantity =  params["noOfBags_#{index}"].to_i * BagType.find_by_id(@lot.bag_type_id).try(:quandity).to_f
            @lot.amc_id = @gate_entry.amc_id
            @lot.gate_entry_pass_no = @gate_entry.gatepass_no
            @lot.amc_id = @gate_entry.amc_id
            @lot.is_gate_entry = true
            @lot.posted_on = Date.today
            @lot.coldstore_id = @gate_entry.cold_storage_id if @gate_entry.cold_storage_id.present?
            @lot.save

              if (@lot.amc_id.equal?(7) || @lot.amc_id.equal?(1))
                puts "RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR"
                @todaylots = Lot.where("amc_id=? and posted_on=?", @lot.amc_id, Date.today).size()
                @lot.update_attribute(:lot_code, "L" + @lot.amc_id.to_s + Date.today.strftime("%Y%m%d") + @todaylots.to_s.rjust(4, "0"))
              else
                @lot.update_attribute(:lot_code, "L" +'-' +@lot.id.to_s)
              end
            end
          end
        end

        format.html { redirect_to "/gate_entries/show_lots?gate_entry_id=#{@gate_entry.id}", notice: 'Gate entry was successfully created.' }
        format.json { render json: @gate_entry, status: :created, location: @gate_entry }
      else
        format.html { render action: "new" }
        format.json { render json: @gate_entry.errors, status: :unprocessable_entity }
      end


    end
  end



  def show_lots
    @gate_entry = GateEntry.find_by_id(params[:gate_entry_id]) if params[:gate_entry_id]
    @lots = Lot.find_all_by_gate_entry_pass_no(@gate_entry.gatepass_no)

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "AMC#{@gate_entry.gatepass_no}",
               :page_height => '6in',
               :page_width => '3in',
               :margin => {:top                => 5,
                           :bottom             => 5,
                           :left               => 5,
                           :right              => 5},
               :orientation      => 'portrait'
      end
    end


  end

  def create_gate_pass
    @gate_entry = GateEntry.new
    @gate_entry.amc_id = current_user.amc_id
    @gate_entry.gatepass_no = Date.today
    respond_to do |format|
      if @gate_entry.save
        @gate_entry.gatepass_no = @gate_entry.gatepass_no.to_s+'-'+@gate_entry.amc_id.to_s+'-'+@gate_entry.id.to_s
        @gate_entry.save
        format.html { redirect_to "/lots/new?gate_entry_id=#{@gate_entry.id}", notice: 'Gate entry was successfully created.' }
        format.json { render json: @gate_entry, status: :created, location: @gate_entry }
      else
        format.html { render action: "new" }
        format.json { render json: @gate_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /gate_entries/1
  # PUT /gate_entries/1.json
  def update
    @gate_entry = GateEntry.find(params[:id])

    respond_to do |format|
      if @gate_entry.update_attributes(params[:gate_entry])
        format.html { redirect_to "/gate_entries", notice: 'Gate entry was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gate_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gate_entries/1
  # DELETE /gate_entries/1.json
  def destroy
    @gate_entry = GateEntry.find(params[:id])
    @gate_entry.destroy

    respond_to do |format|
      format.html { redirect_to gate_entries_url }
      format.json { head :no_content }
    end
  end


  def get_farmers
    session[SessionConstants::LAST_CREATED_FARMER] = ''

    if current_user.present? && current_user.amc_id.present?
      @farmers = Farmer.name_scope(params[:name]).mobile_no_scope(params[:mobile_number]).village_scope(params[:village]).amcscope(current_user.amc_id).reverse
    else
      @farmers = Farmer.name_scope(params[:name]).mobile_no_scope(params[:mobile_number]).village_scope(params[:village]).reverse
    end

  end

  def generate_gate_entries
    @entryDetails = JSON.parse(params[:gateEntryHeader]) if params[:gateEntryHeader].present?
    @lotDetails = JSON.parse(params[:lotDetails]) if params[:lotDetails].present?
    @list_lots = []

    if @entryDetails.present? && @lotDetails.present?
      @gate_entry = GateEntry.new
      @gate_entry.amc_id = @entryDetails["oprId"].to_i
      @gate_entry.gatepass_no = "G"
      @gate_entry.entry_time = @entryDetails["gateEntryDate"]
      @gate_entry.posted_on = @entryDetails["gateEntryDate"]
      @gate_entry.created_by = @entryDetails["userId"]
      @gate_entry.device_id = @entryDetails["deviceId"]
      @gate_entry.deviceType = @entryDetails["deviceType"]
      @gate_entry.gate_entry_type = @entryDetails["getGateEntryType"]
      @gate_entry.save
      @gate_entry.update_attribute(:gatepass_no, @gate_entry.gatepass_no.to_s + "-" + @gate_entry.id.to_s )

      if @lotDetails.present? && @gate_entry.present?
        @lotDetails.each do |lot|
          @gate_entry.update_attribute(:vechile_no, lot["vehicleNo"])
          @gate_entry.update_attribute(:vehicle_type_id, lot["vehicleTypeId"])
          @lot = Lot.new
          @lot.seller_type = lot["sellerType"]
          @lot.seller_id = lot["sellerId"].blank? ? @farmer.id : lot["sellerId"]
          @lot.mobile_no = "8008446701"
          @lot.commodity_id = lot["commodityId"]
          @lot.bag_type_id = lot["bagTypeId"]
          @lot.no_of_bags = lot["noOfBags"]
          @lot.vehicle_type_id = lot["vehicleTypeId"]
          @lot.vechile_no = lot["vehicleNo"]
          @lot.aprox_quantity = lot["aproxQuantity"]
          @lot.ca_name = lot["caId"]
          @lot.gate_entry_pass_no = @gate_entry.gatepass_no
          @lot.amc_id = @gate_entry.amc_id
          @lot.is_gate_entry = true
          @lot.posted_on = Date.today
          @lot.save
          @lots = Lot.find_all_by_amc_id_and_posted_on(@lot.amc_id,Date.today)
          @lot.update_attribute(:lot_code, "L" + @lot.amc_id.to_s + Date.today.strftime("%Y%m%d") + @lots.size.to_s.rjust(4, "0"))
          @list_lots << @lot.lot_code
        end
      end
      @details = FinalResponse.new('S', '', @list_lots.join(",") , @gate_entry.gatepass_no)
    else
      @details = FinalResponse.new('F', 'S', @list_lots, "")
    end


    respond_to do |format|
      if if @gate_entry.present?
           format.json {render json: @details}
         else
           format.json {render :json => {:error_message => "NOT AVAILABLE"}}
         end
      end
    end

  end

  def generate_gate_entry

  end


  def set_farmer_details
      session[SessionConstants::SELECTED_ID] = ''
      @farmer = Farmer.find_by_contact_no(params[:name].split('--')[1]) if params[:name].present?
      session[SessionConstants::SELECTED_ID] =  params[:id] if params[:id].present?
  end

end
