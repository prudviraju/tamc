class FeeComponentsController < ApplicationController
  # GET /fee_components
  # GET /fee_components.json
  def index
    @fee_components = FeeComponent.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @fee_components }
    end
  end

  # GET /fee_components/1
  # GET /fee_components/1.json
  def show
    @fee_component = FeeComponent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @fee_component }
    end
  end

  # GET /fee_components/new
  # GET /fee_components/new.json
  def new
    @fee_component = FeeComponent.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @fee_component }
    end
  end

  # GET /fee_components/1/edit
  def edit
    @fee_component = FeeComponent.find(params[:id])
  end

  # POST /fee_components
  # POST /fee_components.json
  def create
    @fee_component = FeeComponent.new(params[:fee_component])
    @fee_component.modified_on = Time.now
    respond_to do |format|
      if @fee_component.save
        format.html { redirect_to '/fee_components', notice: 'Fee component was successfully created.' }
        format.json { render json: @fee_component, status: :created, location: @fee_component }
      else
        format.html { render action: "new" }
        format.json { render json: @fee_component.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /fee_components/1
  # PUT /fee_components/1.json
  def update
    @fee_component = FeeComponent.find(params[:id])
    @fee_component.modified_on = Time.now
    respond_to do |format|
      if @fee_component.update_attributes(params[:fee_component])
        format.html { redirect_to '/fee_components', notice: 'Fee component was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @fee_component.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fee_components/1
  # DELETE /fee_components/1.json
  def destroy
    @fee_component = FeeComponent.find(params[:id])
    @fee_component.destroy

    respond_to do |format|
      format.html { redirect_to fee_components_url }
      format.json { head :no_content }
    end
  end
end
