class BagTypesController < ApplicationController
  # GET /bag_types
  # GET /bag_types.json
  def index
    if current_user.present? && current_user.amc_id.present?
      @bag_types = BagType.where("amc_id =?",current_user.amc_id)
    else
      @bag_types = BagType.all
    end


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bag_types }
    end
  end

  # GET /bag_types/1
  # GET /bag_types/1.json
  def show
    @bag_type = BagType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bag_type }
    end
  end

  # GET /bag_types/new
  # GET /bag_types/new.json
  def new
    @bag_type = BagType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bag_type }
    end
  end

  # GET /bag_types/1/edit
  def edit
    @bag_type = BagType.find(params[:id])
  end

  # POST /bag_types
  # POST /bag_types.json
  def create
    @bag_type = BagType.new(params[:bag_type])

    respond_to do |format|
      if @bag_type.save
        format.html { redirect_to '/bag_types', notice: 'Bag type was successfully created.' }
        format.json { render json: @bag_type, status: :created, location: @bag_type }
      else
        format.html { render action: "new" }
        format.json { render json: @bag_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /bag_types/1
  # PUT /bag_types/1.json
  def update
    @bag_type = BagType.find(params[:id])

    respond_to do |format|
      if @bag_type.update_attributes(params[:bag_type])
        format.html { redirect_to '/bag_types', notice: 'Bag type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @bag_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bag_types/1
  # DELETE /bag_types/1.json
  def destroy
    @bag_type = BagType.find(params[:id])
    @bag_type.destroy

    respond_to do |format|
      format.html { redirect_to bag_types_url }
      format.json { head :no_content }
    end
  end
end
