class SubLotsController < ApplicationController
  # GET /sub_lots
  # GET /sub_lots.json
  def index
    @sub_lots = SubLot.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sub_lots }
    end
  end

  # GET /sub_lots/1
  # GET /sub_lots/1.json
  def show
    @sub_lot = SubLot.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sub_lot }
    end
  end

  # GET /sub_lots/new
  # GET /sub_lots/new.json
  def new
    @sub_lot = SubLot.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sub_lot }
    end
  end

  # GET /sub_lots/1/edit
  def edit
    @sub_lot = SubLot.find(params[:id])
  end

  # POST /sub_lots
  # POST /sub_lots.json
  def create
    @sub_lot = SubLot.new(params[:sub_lot])

    respond_to do |format|
      if @sub_lot.save
        format.html { redirect_to @sub_lot, notice: 'Sub lot was successfully created.' }
        format.json { render json: @sub_lot, status: :created, location: @sub_lot }
      else
        format.html { render action: "new" }
        format.json { render json: @sub_lot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sub_lots/1
  # PUT /sub_lots/1.json
  def update
    @sub_lot = SubLot.find(params[:id])

    respond_to do |format|
      if @sub_lot.update_attributes(params[:sub_lot])
        format.html { redirect_to @sub_lot, notice: 'Sub lot was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sub_lot.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sub_lots/1
  # DELETE /sub_lots/1.json
  def destroy
    @sub_lot = SubLot.find(params[:id])
    @sub_lot.destroy

    respond_to do |format|
      format.html { redirect_to sub_lots_url }
      format.json { head :no_content }
    end
  end
end
