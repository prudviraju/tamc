class FeeCategoriesController < ApplicationController
  # GET /fee_categories
  # GET /fee_categories.json
  include FeeCategoriesHelper
  def index
    @fee_categories = FeeCategory.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @fee_categories }
    end
  end

  # GET /fee_categories/1
  # GET /fee_categories/1.json
  def show
    @fee_category = FeeCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @fee_category }
    end
  end

  # GET /fee_categories/new
  # GET /fee_categories/new.json
  def new
    @fee_category = FeeCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @fee_category }
    end
  end

  # GET /fee_categories/1/edit
  def edit
    @fee_category = FeeCategory.find(params[:id])
  end

  # POST /fee_categories
  # POST /fee_categories.json
  def create
    @fee_category = FeeCategory.new(params[:fee_category])

    respond_to do |format|
      if @fee_category.save
        format.html { redirect_to '/fee_categories', notice: 'Fee category was successfully created.' }
        format.json { render json: @fee_category, status: :created, location: @fee_category }
      else
        format.html { render action: "new" }
        format.json { render json: @fee_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /fee_categories/1
  # PUT /fee_categories/1.json
  def update
    @fee_category = FeeCategory.find(params[:id])

    respond_to do |format|
      if @fee_category.update_attributes(params[:fee_category])
        format.html { redirect_to '/fee_categories', notice: 'Fee category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @fee_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fee_categories/1
  # DELETE /fee_categories/1.json
  def destroy
    @fee_category = FeeCategory.find(params[:id])
    @fee_category.destroy

    respond_to do |format|
      format.html { redirect_to fee_categories_url }
      format.json { head :no_content }
    end
  end


  def listFeeCategoryForAutoSaleModel

    @fee_categories = FeeCategory.where('amc_id =? and commodity_id=?',params[:oprId],params[:commodityId])
    @categories= []
    if @fee_categories.present?
      @fee_categories.each do |category|
        @categories << FeeCategoriesInformation.new(Amc.find_by_id(category.amc_id).try(:state_id),category.amc_id,category.id, category.category_name)
      end
      @details = FinalFeeCategoriesInformation.new('S', @categories)
    end
    respond_to do |format|
      if @fee_categories.present?
        format.json {render json: @details}
      else
        format.json {render :json => {:error_message => "NOT AVAILABLE"}}
      end
    end

  end

  def listFeeComponentMstDtlAutoSaleModel
    @fee_components = FeeComponent.where('amc_id =? and fee_category_id=?',params[:oprId],params[:feeCategoryId])
    @components= []
    if @fee_components.present?
      @fee_components.each do |component|
        @components << FeeComponentInformation.new(component.id,component.name,component.fee_type, component.party_type)
      end
      @details = FinalFeeComponentsInformation.new('S', @components)
    end
    respond_to do |format|
      if @fee_components.present?
        format.json {render json: @details}
      else
        format.json {render :json => {:error_message => "NOT AVAILABLE"}}
      end
    end
  end
end
