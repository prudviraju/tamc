class BagNamesController < ApplicationController
  # GET /bag_names
  # GET /bag_names.json
  def index
    @bag_names = BagName.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bag_names }
    end
  end

  # GET /bag_names/1
  # GET /bag_names/1.json
  def show
    @bag_name = BagName.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bag_name }
    end
  end

  # GET /bag_names/new
  # GET /bag_names/new.json
  def new
    @bag_name = BagName.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bag_name }
    end
  end

  # GET /bag_names/1/edit
  def edit
    @bag_name = BagName.find(params[:id])
  end

  # POST /bag_names
  # POST /bag_names.json
  def create
    @bag_name = BagName.new(params[:bag_name])

    respond_to do |format|
      if @bag_name.save
        format.html { redirect_to '/bag_names', notice: 'Bag name was successfully created.' }
        format.json { render json: @bag_name, status: :created, location: @bag_name }
      else
        format.html { render action: "new" }
        format.json { render json: @bag_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /bag_names/1
  # PUT /bag_names/1.json
  def update
    @bag_name = BagName.find(params[:id])

    respond_to do |format|
      if @bag_name.update_attributes(params[:bag_name])
        format.html { redirect_to '/bag_names', notice: 'Bag name was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @bag_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bag_names/1
  # DELETE /bag_names/1.json
  def destroy
    @bag_name = BagName.find(params[:id])
    @bag_name.destroy

    respond_to do |format|
      format.html { redirect_to bag_names_url }
      format.json { head :no_content }
    end
  end


end
