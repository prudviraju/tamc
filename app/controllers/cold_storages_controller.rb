class ColdStoragesController < ApplicationController
  # GET /cold_storages
  # GET /cold_storages.json
  def index

    if current_user.present? && current_user.amc_id.present?
      @cold_storages = ColdStorage.where("amc_id =?",current_user.amc_id)
    else
      @cold_storages = ColdStorage.all
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @cold_storages }
    end
  end

  # GET /cold_storages/1
  # GET /cold_storages/1.json
  def show
    @cold_storage = ColdStorage.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @cold_storage }
    end
  end

  # GET /cold_storages/new
  # GET /cold_storages/new.json
  def new
    @cold_storage = ColdStorage.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @cold_storage }
    end
  end

  # GET /cold_storages/1/edit
  def edit
    @cold_storage = ColdStorage.find(params[:id])
  end

  # POST /cold_storages
  # POST /cold_storages.json
  def create
    @cold_storage = ColdStorage.new(params[:cold_storage])

    respond_to do |format|
      if @cold_storage.save
        format.html { redirect_to '/cold_storages', notice: 'Cold storage was successfully created.' }
        format.json { render json: @cold_storage, status: :created, location: @cold_storage }
      else
        format.html { render action: "new" }
        format.json { render json: @cold_storage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /cold_storages/1
  # PUT /cold_storages/1.json
  def update
    @cold_storage = ColdStorage.find(params[:id])

    respond_to do |format|
      if @cold_storage.update_attributes(params[:cold_storage])
        format.html { redirect_to '/cold_storages', notice: 'Cold storage was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cold_storage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cold_storages/1
  # DELETE /cold_storages/1.json
  def destroy
    @cold_storage = ColdStorage.find(params[:id])
    @cold_storage.destroy

    respond_to do |format|
      format.html { redirect_to cold_storages_url }
      format.json { head :no_content }
    end
  end
end
