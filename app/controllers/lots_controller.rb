class LotsController < ApplicationController
  # GET /lots
  # GET /lots.json
  include LotsHelper
  def index
    @limit = !params[:limit].blank? ? params[:limit] : 15
    @offset = !params[:offset].blank? ? params[:offset] : 0

    if current_user.present? && current_user.amc_id.present?

      if params[:seller_id].blank? && params[:posted_on].blank? && params[:lot_code].blank? && params[:vechile_no].blank?
        @lotCount = Lot.where('amc_id=? and is_gate_entry=? and is_weighment =? and posted_on=? and is_active =?', current_user.amc_id, true, false,Date.today,true).size
        @lots = Lot.where('amc_id=? and is_gate_entry=? and is_weighment =? and posted_on=? and is_active =?', current_user.amc_id, true, false,Date.today,true).limit(@limit).offset(@offset).reverse
      else
        @lotCount = Lot.where('amc_id=? and is_gate_entry=? and is_weighment =? and is_active =?', current_user.amc_id, true, false,true).seller_scope(params[:seller_id]).posted_scope(params[:posted_on]).lot_code_scope(params[:lot_code]).commodiy_scope(params[:commodity_id]).ca_scope(params[:ca_id]).size
        @lots = Lot.where('amc_id=? and is_gate_entry=? and is_weighment =? and is_active =?', current_user.amc_id, true, false,true).seller_scope(params[:seller_id]).posted_scope(params[:posted_on]).lot_code_scope(params[:lot_code]).commodiy_scope(params[:commodity_id]).ca_scope(params[:ca_id]).limit(@limit).offset(@offset).reverse
      end
    else
      @lotCount = Lot.all.size
      @lots = Lot.all.limit(@limit).offset(@offset)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.js
      format.json { render json: @lots }
    end
  end


  def wayment_done_lots

    @limit = !params[:limit].blank? ? params[:limit] : 15
    @offset = !params[:offset].blank? ? params[:offset] : 0

    if current_user.present? && current_user.amc_id.present?

      if params[:seller_id].blank? && params[:posted_on].blank? && params[:lot_code].blank? && params[:vechile_no].blank?
        @lotCount = Lot.where('amc_id=? and is_gate_entry=? and is_weighment =? and is_takpatti=? and posted_on=?', current_user.amc_id, true, true, true, Date.today).size
        @lots = Lot.where('amc_id=? and is_gate_entry=? and is_weighment =? and is_takpatti=? and posted_on=?', current_user.amc_id, true, true, true, Date.today).limit(@limit).offset(@offset).reverse
      else
        @lotCount = Lot.where('amc_id=? and is_gate_entry=? and is_weighment =? and is_takpatti=?', current_user.amc_id, true, true, true).trader_scope(params[:ta_id]).posted_scope(params[:posted_on]).lot_code_scope(params[:lot_code]).commodiy_scope(params[:commodity_id]).ca_scope(params[:ca_id]).size
        @lots = Lot.where('amc_id=? and is_gate_entry=? and is_weighment =? and is_takpatti=?', current_user.amc_id, true, true, true).trader_scope(params[:ta_id]).posted_scope(params[:posted_on]).lot_code_scope(params[:lot_code]).commodiy_scope(params[:commodity_id]).ca_scope(params[:ca_id]).limit(@limit).offset(@offset).reverse
      end
    else
      @lotCount = Lot.limit(@limit).offset(@offset).size
      @lots = Lot.limit(@limit).offset(@offset).reverse
    end

    respond_to do |format|
      format.html # index.html.erb
      format.js
      format.json {render json: @lots}
    end
  end

  def show_entry_ticket
    @lot = Lot.find_by_id(params[:lot_id])

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "AMC#{@lot.lot_code}",
               :page_height => '5in',
               :page_width => '3in',
               :margin => {:top                => 5,
                           :bottom             => 5,
                           :left               => 5,
                           :right              => 5},
               :orientation      => 'portrait'
      end
    end
  end


  def takparty_receipt
    @lot = Lot.find_by_id(params[:lot_id])
    @bids = Bid.find_all_by_lot_id(@lot.id)
    @bag_weights = BagWeight.where("lot_id=?",@lot.id)
    @sublots = SubLot.find_all_by_lot_id(@lot.id)
    @farmer_deductions = LotDeductions.find_all_by_lot_id_and_deduction_type(@lot.id, 'INCLUSIVE')
    @trader_payables = LotDeductions.find_all_by_lot_id_and_deduction_type(@lot.id, 'EXCLUSIVE')

    respond_to do |format|
      if params[:print].present?
        format.html {render :layout => false}
      else
        format.html
      end

      format.pdf do
        render pdf: "AMC_#{@lot.id}",
               :margin => {:top                => 5,
                           :bottom             => 5,
                           :left               => 0,
                           :right              => 0},
               :orientation      => 'portrait'
      end
    end
  end


  def bidding_lots_list

      @lots = Lot.find_all_by_amc_id_and_is_gate_entry_and_is_bid_rigistred(current_user.amc_id,true,false)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lots }
    end
  end

  def add_bid
    @lot = Lot.find_by_id(params[:lot_id]) if params[:lot_id].present?

  end

  def update_bid
    @lot = Lot.find_by_id(params[:lot_id])
    @lot.bid_raise_by = params[:bid_raise_by]
    @lot.bid_amount = params[:bid_amount]
    @lot.is_bid_rigistred = true
    @lot.save
    redirect_to '/lots'
  end

  def close_bidding
    @lot = Lot.find_by_id(params[:lot_id])
    @lot.is_bid_rigistred = true
    @lot.save
    @bid = Bid.where("lot_id =?",@lot.id).order('bid_amount ASC').last
    @bid.is_approved = true
    @bid.save
    redirect_to '/lots'
  end



  # GET /lots/1
  # GET /lots/1.json
  def show
    @lot = Lot.find(params[:id])
    @bids = Bid.find_all_by_lot_id(@lot.id)
    @bag_weights = BagWeight.where("lot_id=?",@lot.id)
    @sublots = SubLot.find_all_by_lot_id(@lot.id)
    @farmer_deductions = LotDeductions.find_all_by_lot_id_and_deduction_type(@lot.id, 'INCLUSIVE')
    @trader_payables = LotDeductions.find_all_by_lot_id_and_deduction_type(@lot.id, 'EXCLUSIVE')

    # @farmer_deductions = LotDeductions.find_all_by_lot_id_and_deduction_type(@lot.id,'INCLUSIVE')
    # @trader_payables = LotDeductions.find_all_by_lot_id_and_deduction_type(@lot.id,'EXCLUSIVE')

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lot }
    end
  end

  # GET /lots/new
  # GET /lots/new.json
  def new
    @lot = Lot.new
    @gate_entry = GateEntry.find_by_id(params[:gate_entry_id])
    @lots = Lot.find_all_by_gate_entry_pass_no(@gate_entry.gatepass_no) if @gate_entry.present?
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lot }
    end
  end

  # GET /lots/1/edit
  def edit
    @lot = Lot.find(params[:id])
  end

  # POST /lots
  # POST /lots.json
  def create
    @lot = Lot.new(params[:lot])

    respond_to do |format|
      if @lot.save
        @lot.lot_code = "L" + @lot.amc_id.to_s + '-' +@lot.id.to_s
        @lot.is_gate_entry = true
        @lot.posted_on = Date.today
        @lot.save
        format.html { redirect_to "/lots/new?gate_entry_id=#{params[:gate_entry_id]}", notice: 'Lot was successfully created.' }
        format.json { render json: @lot, status: :created, location: @lot }
      else
        format.html { render action: "new" }
        format.json { render json: @lot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lots/1
  # PUT /lots/1.json
  def update
    @lot = Lot.find(params[:id])


    @farmer = Farmer.find_by_name_and_amc_id(params[:farmerName],current_user.amc_id)

    if @farmer.present?
      @lot.seller_id = @farmer.id
      @lot.state = @farmer.state
      @lot.mandal = @farmer.mandal
      @lot.village = @farmer.village
      @lot.district = @farmer.district
    else
      @farmer = Farmer.new
      @farmer.name = params[:farmerName]
      @farmer.save
      @lot.seller_id = @farmer.id
    end

    respond_to do |format|
      if @lot.update_attributes(params[:lot])

        @lot.aprox_quantity =  @lot.no_of_bags.to_i * BagType.find_by_id(@lot.bag_type_id).try(:quandity).to_f
        @lot.ca_firm = User.find_by_id(@lot.ca_name.to_i).try(:company_name)
        @lot.save
        format.html { redirect_to @lot, notice: 'Lot was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lot.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lots/1
  # DELETE /lots/1.json
  def destroy
    @lot = Lot.find(params[:id])
    @lot.destroy

    redirect_to '/lots'
  end

  def make_inactive
    @lot = Lot.find_by_id(params[:lot_id])
    @lot.update_attribute(:is_active,false)
    redirect_to '/lots'
  end

  def makeDelete
    @lot = Lot.find_by_id(params[:lot_id])
    @lot.destroy
    redirect_to '/lots'
  end

  def modify_lot
   @lot = Lot.find_by_id(params[:lot_id])
  end

  def lot_updated
    @lot = Lot.find_by_id(params[:lot_id])
    @lot.bid_amount = params[:bid_amount]
    @lot.save
  end


  def lots_information

    if current_user.present? && current_user.amc_id.present?
      @from_date = params[:from_date].present? ? params[:from_date] : Date.today
      @to_date = params[:to_date].present? ? params[:to_date] : Date.today
      @lots = Lot.where('amc_id=? and is_gate_entry=? and is_weighment =? and is_active =?', current_user.amc_id, true, true, true).trader_scope(params[:bid_raise_by]).posted_scope1(@from_date,@to_date).commodiy_scope(params[:commodity_id]).ca_scope(params[:ca_id]).bid_scope(params[:rate],params[:rate1]).cold_scope(params[:coldstore_id]).order('posted_on ASC')
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def arrivedLotsInformation

    if current_user.present? && current_user.amc_id.present?
      @from_date = params[:from_date].present? ? params[:from_date] : Date.today
      @to_date = params[:to_date].present? ? params[:to_date] : Date.today
      @lots = Lot.where('amc_id=?', current_user.amc_id).posted_scope1(@from_date,@to_date).commodiy_scope(params[:commodity_id]).ca_scope(params[:ca_id]).cold_scope(params[:coldstore_id]).reverse
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end


  def pendingLotsInformation
    if current_user.present? && current_user.amc_id.present?
      @from_date = params[:from_date].present? ? params[:from_date] : Date.today
      @to_date = params[:to_date].present? ? params[:to_date] : Date.today
      @lots = Lot.where('amc_id=? and is_gate_entry=?  and is_active =? and is_weighment =? ', current_user.amc_id, true, true,false ).posted_scope1(@from_date,@to_date).commodiy_scope(params[:commodity_id]).ca_scope(params[:ca_id]).cold_scope(params[:coldstore_id]).reverse
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end



  def returnedLotsInformation
    if current_user.present? && current_user.amc_id.present?
      @from_date = params[:from_date].present? ? params[:from_date] : Date.today
      @to_date = params[:to_date].present? ? params[:to_date] : Date.today
      @lots = Lot.where('amc_id=? and is_gate_entry=?  and is_active =? and is_weighment =? ', current_user.amc_id, true, false,false ).posted_scope1(@from_date,@to_date).commodiy_scope(params[:commodity_id]).ca_scope(params[:ca_id]).cold_scope(params[:coldstore_id]).reverse
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end


  def day_wise_summary
    @summary_data = {}
    if current_user.present? && current_user.amc_id.present?
      @from_date = params[:from_date].present? ? Date.parse(params[:from_date]) : Date.today
      @to_date = params[:to_date].present? ? Date.parse(params[:to_date]) : Date.today
      (@from_date).upto(@to_date).each_with_index do |day, index|
        @summary_data[day] = {}
        @summary_data[day]['sNo'] = index + 1
        @summary_data[day]['date'] = day
        @summary_data[day]['arrivedLots'] = ActiveRecord::Base.connection.execute("select count(*) from lots where posted_on ='#{day.to_date}' and amc_id ='#{current_user.amc_id}' and coldstore_id IS NULL").first[0]
        @summary_data[day]['arrivedBags'] = ActiveRecord::Base.connection.execute("select sum(no_of_bags) from lots where posted_on ='#{day.to_date}' and amc_id ='#{current_user.amc_id}' and coldstore_id IS NULL").first[0]
        @summary_data[day]['weighmentLots'] = ActiveRecord::Base.connection.execute("select count(*) from lots where posted_on ='#{day.to_date}' and amc_id ='#{current_user.amc_id}' and is_weighment=true and coldstore_id IS NULL").first[0]
        @summary_data[day]['weighmentBags'] = ActiveRecord::Base.connection.execute("select sum(no_of_bags) from lots where posted_on ='#{day.to_date}' and amc_id ='#{current_user.amc_id}' and is_weighment=true and coldstore_id IS NULL").first[0]
        @summary_data[day]['weighmentedBags'] = ActiveRecord::Base.connection.execute("select sum(finalised_bag_count) from lots where posted_on ='#{day.to_date}' and amc_id ='#{current_user.amc_id}' and is_weighment=true and coldstore_id IS NULL").first[0]
        @summary_data[day]['weighmentedpendingBags'] = ActiveRecord::Base.connection.execute("select abs(sum(no_of_bags) - sum(finalised_bag_count)) from lots where posted_on ='#{day.to_date}' and amc_id ='#{current_user.amc_id}' and is_weighment=true and coldstore_id IS NULL").first[0]
        @summary_data[day]['pendingLots'] = ActiveRecord::Base.connection.execute("select count(*) from lots where posted_on ='#{day.to_date}' and amc_id ='#{current_user.amc_id}' and is_weighment=false and is_active=true and coldstore_id IS NULL").first[0]
        @summary_data[day]['pendingBags'] = ActiveRecord::Base.connection.execute("select sum(no_of_bags) from lots where posted_on ='#{day.to_date}' and amc_id ='#{current_user.amc_id}' and is_weighment=false and is_active=true and coldstore_id IS NULL").first[0]
        @summary_data[day]['returnedLots'] = ActiveRecord::Base.connection.execute("select count(*) from lots where posted_on ='#{day.to_date}' and amc_id ='#{current_user.amc_id}' and is_weighment=false and is_active=false and coldstore_id IS NULL").first[0]
        @summary_data[day]['returnedBags'] = ActiveRecord::Base.connection.execute("select sum(no_of_bags) from lots where posted_on ='#{day.to_date}' and amc_id ='#{current_user.amc_id}' and is_weighment=false and is_active=false and coldstore_id IS NULL").first[0]
      end

    end

    respond_to do |format|
      format.html
      format.js
      format.xls
      format.json {render json: @summary_data}
    end
  end

  def day_wise_summary1
    @summary_data = {}
    if current_user.present? && current_user.amc_id.present?
      if params[:cold_storage].present?
      @coldstorages = ColdStorage.where("id =?", params[:cold_storage])
      else
        @coldstorages = ColdStorage.where("amc_id =?", current_user.amc_id)
      end

      @from_date = params[:from_date].present? ? params[:from_date] : Date.today
      @to_date = params[:to_date].present? ? Date.parse(params[:to_date]) : Date.today
      @coldstorages.each_with_index do |cold, index|
        @summary_data[cold.id] = {}
        @summary_data[cold.id]['sNo'] = index + 1
        @summary_data[cold.id]["NAME"] = cold.name
        @summary_data[cold.id]['arrivedLots'] = ActiveRecord::Base.connection.execute("SELECT count(*) FROM TAMC_production.lots where gate_entry_pass_no in(select gatepass_no from gate_entries where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and cold_storage_id ='#{cold.id}')").first[0]
        @summary_data[cold.id]['arrivedBags'] = ActiveRecord::Base.connection.execute("SELECT sum(no_of_bags) FROM TAMC_production.lots where gate_entry_pass_no in(select gatepass_no from gate_entries where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and cold_storage_id ='#{cold.id}')").first[0]
        @summary_data[cold.id]['weighmentLots'] = ActiveRecord::Base.connection.execute("SELECT count(*) FROM TAMC_production.lots where gate_entry_pass_no in(select gatepass_no from gate_entries where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and cold_storage_id ='#{cold.id}')and is_bid_rigistred=true and is_weighment=true" ).first[0]
        @summary_data[cold.id]['weighmentBags'] = ActiveRecord::Base.connection.execute("SELECT sum(no_of_bags) FROM TAMC_production.lots where gate_entry_pass_no in(select gatepass_no from gate_entries where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and cold_storage_id ='#{cold.id}')and is_bid_rigistred=true and is_weighment=true" ).first[0]
        @summary_data[cold.id]['pendingLots'] = ActiveRecord::Base.connection.execute("SELECT count(*) FROM TAMC_production.lots where gate_entry_pass_no in(select gatepass_no from gate_entries where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and cold_storage_id ='#{cold.id}') and is_weighment=false and is_active=true" ).first[0]
        @summary_data[cold.id]['pendingBags'] = ActiveRecord::Base.connection.execute("SELECT sum(no_of_bags) FROM TAMC_production.lots where gate_entry_pass_no in(select gatepass_no from gate_entries where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and cold_storage_id ='#{cold.id}')  and is_weighment=false and is_active=true" ).first[0]
        @summary_data[cold.id]['returnedLots'] = ActiveRecord::Base.connection.execute("SELECT count(*) FROM TAMC_production.lots where gate_entry_pass_no in(select gatepass_no from gate_entries where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and cold_storage_id ='#{cold.id}') and is_active=false").first[0]
        @summary_data[cold.id]['returnedBags'] = ActiveRecord::Base.connection.execute("SELECT sum(no_of_bags) FROM TAMC_production.lots where gate_entry_pass_no in(select gatepass_no from gate_entries where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and cold_storage_id ='#{cold.id}') and is_active=false").first[0]
      end
    end

    respond_to do |format|
      format.html
      format.js
      format.xls
      format.json {render json: @summary_data}
    end
  end


  def market_fee_details
    @summary_data = {}
    if current_user.present? && current_user.amc_id.present?
      @from_date = params[:from_date].present? ? Date.parse(params[:from_date]) : Date.today
      @to_date = params[:to_date].present? ? Date.parse(params[:to_date]) : Date.today

      if params[:bid_raise_by].present?
        @summary_data = ActiveRecord::Base.connection.execute("SELECT u.id,u.name,sum(l.gross_amount),sum(l.market_charges),count(l.id) ,sum(l.finalised_bag_count),sum(l.netWeight), u.company_name
      FROM TAMC_production.lots l
       inner join users u on u.id = '#{params[:bid_raise_by]}' and u.amc_id='#{current_user.amc_id}' and u.user_role='Trader'
       where l.amc_id ='#{current_user.amc_id}'
       and l.posted_on between '#{@from_date}' and '#{@to_date}'
       and l.is_weighment =true
       and l.is_active = true
       and l.is_active = true
       and l.bid_raise_by ='#{params[:bid_raise_by]}'
       ")

      else
        @summary_data = ActiveRecord::Base.connection.execute("SELECT u.id,u.name,sum(l.gross_amount),sum(l.market_charges),count(l.id) ,sum(l.finalised_bag_count),sum(l.netWeight), u.company_name
      FROM TAMC_production.lots l
       inner join users u on u.id =l.bid_raise_by and u.amc_id='#{current_user.amc_id}' and u.user_role='Trader'
       where l.amc_id ='#{current_user.amc_id}'
       and l.posted_on between '#{@from_date}' and '#{@to_date}'
       and l.is_weighment =true
       and l.is_active = true
       group by u.id")
      end

    end

    respond_to do |format|
      format.html
      format.js
      format.xls
      format.json {render json: @summary_data}
    end
  end

  def detailsOfFee
    @summary_data = {}
    if current_user.present? && current_user.amc_id.present?
      @from_date = params[:from_date].present? ? Date.parse(params[:from_date]) : Date.today
      @to_date = params[:to_date].present? ? Date.parse(params[:to_date]) : Date.today
      @bid_raised_by = params[:bid_raised_by]

      @agents = User.where("amc_id=? and is_admin=? and is_employee =? and user_role=? and is_active =?", current_user.amc_id, false, false, 'CommissionAgent', true)
      @agents.each_with_index do |user, index|
        @summary_data[user.id] = {}
        @summary_data[user.id]["NAME"] = user.name
        @summary_data[user.id]["CNAME"] = user.company_name
        @summary_data[user.id]['GAMOUNT'] = ActiveRecord::Base.connection.execute("SELECT sum(gross_amount) FROM TAMC_production.lots where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and bid_raise_by='#{@bid_raised_by}' and is_weighment=true and ca_name='#{user.id}'").first[0]
        @summary_data[user.id]['MCHARGE'] = ActiveRecord::Base.connection.execute("SELECT sum(market_charges) FROM TAMC_production.lots where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and bid_raise_by='#{@bid_raised_by}' and is_weighment=true and ca_name='#{user.id}'").first[0]
        @summary_data[user.id]['LOTCOUNT'] = ActiveRecord::Base.connection.execute("SELECT count(id) FROM TAMC_production.lots where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and bid_raise_by='#{@bid_raised_by}' and is_weighment=true and ca_name='#{user.id}'").first[0]
        @summary_data[user.id]['BAGCOUNT'] = ActiveRecord::Base.connection.execute("SELECT sum(finalised_bag_count) FROM TAMC_production.lots where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and bid_raise_by='#{@bid_raised_by}' and is_weighment=true and ca_name='#{user.id}'").first[0]
        @summary_data[user.id]['NETWEIGHT'] = ActiveRecord::Base.connection.execute("SELECT sum(netWeight) FROM TAMC_production.lots where posted_on between '#{@from_date}' and '#{@to_date}' and amc_id='#{current_user.amc_id}' and bid_raise_by='#{@bid_raised_by}' and is_weighment=true and ca_name='#{user.id}'").first[0]
      end
    end
    respond_to do |format|
      format.html
      format.js
      format.xls
      format.json {render json: @summary_data}
    end
  end


  def nrlotBidding
    @lot = Lot.find(params[:id])
  end

  def nrBidUpdate

    @lot = Lot.find_by_id(params[:lot_id])
    @lot.commodity_id = params[:commodity_id]
    @lot.bag_type_id = params[:bag_type_id]
    @lot.ca_name = params[:ca_name]
    @lot.bid_raise_by = params[:bid_raise_by]
    @lot.bid_amount = params[:bid_amount]
    @lot.no_of_bags = params[:no_of_bags]
    @lot.save

    gross_weight = 0.0
    net_weight = 0.0
    tare_weight = 0.0
    gross_amount = 0.0
    net_amount = 0.0
    farmerPayble = 0.0
    deduction_amount = 0.0
    ca_charges = 0.0
    market_charge = 0.0

    @bag_type = BagType.find_by_id(@lot.bag_type_id)
    for x in 1..@lot.no_of_bags.to_i
      @bagweight = BagWeight.new
      @bagweight.lot_id = @lot.id
      @bagweight.bagTypeId = @lot.bag_type_id
      @bagweight.sequence = x
      @bagweight.netWeight = @bag_type.quandity.to_d
      @bagweight.grossWeight = @bag_type.quandity.to_d
      @bagweight.tareWeight = 0.0
      @bagweight.price = @lot.bid_amount.to_d
      @bagweight.save
      gross_weight = gross_weight + @bagweight.grossWeight
      net_weight = net_weight + @bagweight.netWeight
      gross_amount = gross_amount + @bagweight.price
      net_amount = net_amount+@bagweight.price

      @market_charges = MarketCharge.where("amc_id=?", @lot.amc_id)
      @market_charges.each do |charge|
        if @bag_type.quandity.to_d .between?(charge.above_weight.to_d, charge.below_weight.to_d)
        @lotDeduction = LotDeductions.new
        @lotDeduction.bagSeqno = x
        @lotDeduction.lot_code = @lot.lot_code
        @lotDeduction.lot_id = @lot.id
        @lotDeduction.gross_Weight = @bag_type.quandity.to_d
        @lotDeduction.tare_weight = 0.0
        @lotDeduction.net_weight = @bag_type.quandity.to_d
        @lotDeduction.bagamount = @lot.bid_amount.to_d
        @lotDeduction.party_type = charge.party_type
        @lotDeduction.charge_type = charge.charge_type
        @lotDeduction.charge_value = charge.charge_value.to_d
        @lotDeduction.deduction_type = charge.deduction_type
        if charge.charge_type == "AMT"
          @lotDeduction.deduction_amount = charge.charge_value.to_d
        else
          @lotDeduction.deduction_amount = (@lotDeduction.bagamount.to_d / 100) * charge.charge_value.to_d
        end
        @lotDeduction.save
        deduction_amount = deduction_amount + @lotDeduction.deduction_amount
        end
      end
    end

    @lot.update_attribute(:grossWeight,gross_weight.to_d )
    @lot.update_attribute(:netWeight, net_weight.to_d)
    @lot.update_attribute(:tareWeight, tare_weight.to_d)
    @lot.update_attribute(:gross_amount, gross_amount.to_d)
    @lot.update_attribute(:net_amount, gross_amount.to_d)
    @amc = Amc.find_by_id(@lot.amc_id)
    if @amc.present?
      ca_charges = (@lot.gross_amount / 100) * @amc.commission_agent_fee
      market_charge = (@lot.gross_amount / 100) * @amc.market_fee
      deduction_amount = deduction_amount + ca_charges
    end
    @lot.update_attribute(:market_charges, market_charge.to_d)
    @lot.update_attribute(:ca_charges, ca_charges.to_d)
    @lot.update_attribute(:trader_payable_amount, gross_amount.to_d + market_charge )
    @lot.update_attribute(:farmer_amount, gross_amount.to_d - deduction_amount.to_d)
    @lot.update_attribute(:transaction_no,Date.today.strftime("%Y%m%d") + @lot.amc_id.to_s + @lot.id.to_s)
    @lot.update_attribute(:is_bid_rigistred, true)
    @lot.update_attribute(:is_weighment, true)
    @lot.update_attribute(:is_takpatti, true)

    redirect_to "/lots/#{@lot.id}"
  end

  def add_remarks
    @lot = Lot.find(params[:id])
  end

  def save_remarks
    @lot = Lot.find_by_id(params[:lot_id])
    @lot.remarks = params[:remarks] if params[:remarks].present?
    @lot.save
    redirect_to "/lots/#{@lot.id}"
  end

  def nrlotBidding1
    @lot = Lot.find(params[:id])
  end

  def nrBidUpdate1

    @lot = Lot.find_by_id(params[:lot_id])
    @lot.commodity_id = params[:commodity_id]
    @lot.bag_type_id = params[:bag_type_id]
    @lot.ca_name = params[:ca_name]
    @lot.bid_raise_by = params[:bid_raise_by]
    @lot.save

    gross_amount = 0.0
    farmerPayble = 0.0
    deduction_amount = 0.0
    ca_charges = 0.0
    market_charge = 0.0
    number_of_bags = 0

    params.keys.each do |key|
      if key.starts_with?('subLOtNoOfBags_')
        split_keys = key.split('_')
        index = key.split('_')[1]
        @sublot = SubLot.new
        @sublot.lot_id = @lot.id
        @sublot.bid_amount = params["SubLotBitAmount_#{index}"]
        @sublot.no_of_bags = params["subLOtNoOfBags_#{index}"]
        @sublot.amc_id = @lot.amc_id
        @sublot.sub_lot_id = @lot.lot_code
        @sublot.save
        gross_weight = 0.0
        net_weight = 0.0
        tare_weight = 0.0
        net_amount = 0.0

        @bag_type = BagType.find_by_id(@lot.bag_type_id)
        for x in 1..@sublot.no_of_bags.to_i
          number_of_bags = number_of_bags.to_i+1
          @bagweight = BagWeight.new
          @bagweight.lot_id = @lot.id
          @bagweight.bagTypeId = @lot.bag_type_id
          @bagweight.sequence = x
          @bagweight.netWeight = @bag_type.quandity.to_d
          @bagweight.grossWeight = @bag_type.quandity.to_d
          @bagweight.tareWeight = 0.0
          @bagweight.price = @sublot.bid_amount.to_d
          @bagweight.save

          gross_weight = gross_weight + @bagweight.grossWeight
          net_weight = net_weight + @bagweight.netWeight
          net_amount = net_amount+@bagweight.price

          @market_charges = MarketCharge.where("amc_id=?", @lot.amc_id)
          @market_charges.each do |charge|
            if @bag_type.quandity.to_d .between?(charge.above_weight.to_d, charge.below_weight.to_d)
              @lotDeduction = LotDeductions.new
              @lotDeduction.bagSeqno = x
              @lotDeduction.lot_code = @lot.lot_code
              @lotDeduction.lot_id = @lot.id
              @lotDeduction.gross_Weight = @bag_type.quandity.to_d
              @lotDeduction.tare_weight = 0.0
              @lotDeduction.net_weight = @bag_type.quandity.to_d
              @lotDeduction.bagamount = @lot.bid_amount.to_d
              @lotDeduction.party_type = charge.party_type
              @lotDeduction.charge_type = charge.charge_type
              @lotDeduction.charge_value = charge.charge_value.to_d
              @lotDeduction.deduction_type = charge.deduction_type
              if charge.charge_type == "AMT"
                @lotDeduction.deduction_amount = charge.charge_value.to_d
              else
                @lotDeduction.deduction_amount = (@lotDeduction.bagamount.to_d / 100) * charge.charge_value.to_d
              end
              @lotDeduction.save
              deduction_amount = deduction_amount + @lotDeduction.deduction_amount
            end
          end
        end
        @sublot.update_attribute(:grossWeight, gross_weight.to_d)
        @sublot.update_attribute(:netWeight, net_weight.to_d)
        @sublot.update_attribute(:tareWeight, tare_weight.to_d)
        @sublot.update_attribute(:net_amount, net_amount.to_d)
        gross_amount += @sublot.net_amount
      end
    end

    @amc = Amc.find_by_id(@lot.amc_id)
    if @amc.present?
      ca_charges = (gross_amount / 100) * @amc.commission_agent_fee
      market_charge = (gross_amount / 100) * @amc.market_fee
      deduction_amount = deduction_amount + ca_charges
    end


    @lot.update_attribute(:gross_amount, gross_amount.to_d)
    @lot.update_attribute(:market_charges, market_charge.to_d)
    @lot.update_attribute(:ca_charges, ca_charges.to_d)
    @lot.update_attribute(:trader_payable_amount, gross_amount.to_d + market_charge )
    @lot.update_attribute(:farmer_amount, gross_amount.to_d - deduction_amount.to_d)
    @lot.update_attribute(:no_of_bags, number_of_bags.to_i)
    @lot.update_attribute(:finalised_bag_count, number_of_bags.to_i)
    @lot.update_attribute(:transaction_no,Date.today.strftime("%Y%m%d") + @lot.amc_id.to_s + @lot.id.to_s)
    @lot.update_attribute(:is_bid_rigistred, true)
    @lot.update_attribute(:is_weighment, true)
    @lot.update_attribute(:is_takpatti, true)

    redirect_to "/lots/#{@lot.id}"
  end


  def getLotNumbers
    @list_lots = []
    @from_date = Date.today
    @back_date = Date.today - 2.days
    @amc_id = params["amc_id"]
    @all_lotInformation = ActiveRecord::Base.connection.execute("select DISTINCT lot_code as lotCode from lots where posted_on between '#{@back_date}' and '#{@from_date}' and amc_id='#{@amc_id}' and is_weighment=false")
    @all_lotInformation.each do |lot|
      @list_lots << LotNumberResponse.new(lot[0])
    end
    render json: @list_lots
  end

  def getlotInformation
    @alldetails = {}
    @lot = Lot.find_by_lot_code(params[:LotCode]) if params[:LotCode].present?
     @emptybag = GunnyDetail.find_by_bag_type_id_and_commodity_id(@lot.bag_type_id,@lot.commodity_id)
    @details = LotResponse.new('S', @lot.id,@lot.lot_code,@lot.commodity_id,Commodity.find_by_id(@lot.commodity_id).try(:name),@lot.no_of_bags,@lot.bag_type_id,BagType.find_by_id(@lot.bag_type_id).try(:description),@lot.vechile_no,@lot.vehicle_type_id,'',@lot.seller_id,Farmer.find_by_id(@lot.seller_id).try(:name),@lot.ca_name,User.find_by_id(@lot.ca_name).try(:company_name),@lot.bid_raise_by,User.find_by_id(@lot.bid_raise_by).try(:company_name),@lot.bid_amount,@emptybag.present? ? @emptybag.tare_weight : "","",Amc.find_by_id(@lot.amc_id).try(:market_fee),Amc.find_by_id(@lot.amc_id).try(:commission_agent_fee),@lot.gunny_amount)

    @alldetails = AllLotInformation.new(@lot.amc_id,'','', 'S','',@details)

    render json: @alldetails
  end




  #Testing
  #
  # def update_data
  #
  #   @district = District.all
  #   @district.each do |dis|
  #     @mandals = Mandal.find_all_by_district_id(dis.id)
  #     @mandals.each do |man|
  #
  #       url = URI.encode("http://52.91.240.40:8080/TSWB/downloader/getVillageByMandalName?districtName=#{dis.name}&mandalName=#{man.name}")
  #       @villageMenu = HTTParty.get(url)
  #       @villageMenu.each do |menu|
  #         @village = Village.new
  #         @village.district_id = dis.id
  #         @village.mandal_id = man.id
  #         @village.name = menu['villageName']
  #         @village.state_id = 1
  #         @village.is_active = true
  #         @village.save
  #       end
  #     end
  #   end
  #
  #   respond_to do |format|
  #     format.json { render json: @districtMenu, status: :loaded}
  #   end
  # end

end
