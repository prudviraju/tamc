class BagWeightsController < ApplicationController
  # GET /bag_weights
  # GET /bag_weights.json
  def index
    @bag_weights = BagWeight.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bag_weights }
    end
  end

  # GET /bag_weights/1
  # GET /bag_weights/1.json
  def show
    @bag_weight = BagWeight.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bag_weight }
    end
  end

  # GET /bag_weights/new
  # GET /bag_weights/new.json
  def new
    @bag_weight = BagWeight.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bag_weight }
    end
  end

  # GET /bag_weights/1/edit
  def edit
    @bag_weight = BagWeight.find(params[:id])
  end

  # POST /bag_weights
  # POST /bag_weights.json
  def create
    @bag_weight = BagWeight.new(params[:bag_weight])

    respond_to do |format|
      if @bag_weight.save
        format.html { redirect_to @bag_weight, notice: 'Bag weight was successfully created.' }
        format.json { render json: @bag_weight, status: :created, location: @bag_weight }
      else
        format.html { render action: "new" }
        format.json { render json: @bag_weight.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /bag_weights/1
  # PUT /bag_weights/1.json
  def update
    @bag_weight = BagWeight.find(params[:id])

    respond_to do |format|
      if @bag_weight.update_attributes(params[:bag_weight])
        format.html { redirect_to @bag_weight, notice: 'Bag weight was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @bag_weight.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bag_weights/1
  # DELETE /bag_weights/1.json
  def destroy
    @bag_weight = BagWeight.find(params[:id])
    @bag_weight.destroy

    respond_to do |format|
      format.html { redirect_to bag_weights_url }
      format.json { head :no_content }
    end
  end
end
