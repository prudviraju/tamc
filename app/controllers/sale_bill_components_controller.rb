class SaleBillComponentsController < ApplicationController
  # GET /sale_bill_components
  # GET /sale_bill_components.json
  def index
    @sale_bill_components = SaleBillComponent.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sale_bill_components }
    end
  end

  # GET /sale_bill_components/1
  # GET /sale_bill_components/1.json
  def show
    @sale_bill_component = SaleBillComponent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sale_bill_component }
    end
  end

  # GET /sale_bill_components/new
  # GET /sale_bill_components/new.json
  def new
    @sale_bill_component = SaleBillComponent.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sale_bill_component }
    end
  end

  # GET /sale_bill_components/1/edit
  def edit
    @sale_bill_component = SaleBillComponent.find(params[:id])
  end

  # POST /sale_bill_components
  # POST /sale_bill_components.json
  def create
    @sale_bill_component = SaleBillComponent.new(params[:sale_bill_component])

    respond_to do |format|
      if @sale_bill_component.save
        format.html { redirect_to @sale_bill_component, notice: 'Sale bill component was successfully created.' }
        format.json { render json: @sale_bill_component, status: :created, location: @sale_bill_component }
      else
        format.html { render action: "new" }
        format.json { render json: @sale_bill_component.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sale_bill_components/1
  # PUT /sale_bill_components/1.json
  def update
    @sale_bill_component = SaleBillComponent.find(params[:id])

    respond_to do |format|
      if @sale_bill_component.update_attributes(params[:sale_bill_component])
        format.html { redirect_to @sale_bill_component, notice: 'Sale bill component was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sale_bill_component.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sale_bill_components/1
  # DELETE /sale_bill_components/1.json
  def destroy
    @sale_bill_component = SaleBillComponent.find(params[:id])
    @sale_bill_component.destroy

    respond_to do |format|
      format.html { redirect_to sale_bill_components_url }
      format.json { head :no_content }
    end
  end
end
