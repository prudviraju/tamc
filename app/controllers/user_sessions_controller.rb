class UserSessionsController < ApplicationController
  include ApplicationHelper

  def new
    @user_session = UserSession.new
    respond_to do |format|
      format.html {render :layout => 'loginapp'}
    end
  end

  def create
    user = User.find_by_email(params[:user_session][:email])
    params[:user_session][:email] = user.email if user.present?
    @user_session = UserSession.new(params[:user_session])
    if @user_session.save
        redirect_to "/gate_entries/new"
    else
      render :action => 'new', :layout => "loginapp"
    end
  end

  def destroy
    @user_session = UserSession.find
    if @user_session.present?

      if current_user.present? && current_user.is_super_admin
        @current_user.update_attribute(:amc_id, '')
      end

      @user_session.destroy
      reset_session
    end
    redirect_to root_url
  end

end
