class SalebillsController < ApplicationController
  # GET /salebills
  # GET /salebills.json
  def index
    @salebills = Salebill.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @salebills }
    end
  end

  # GET /salebills/1
  # GET /salebills/1.json
  def show
    @salebill = Salebill.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @salebill }
    end
  end

  # GET /salebills/new
  # GET /salebills/new.json
  def new
    @salebill = Salebill.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @salebill }
    end
  end

  # GET /salebills/1/edit
  def edit
    @salebill = Salebill.find(params[:id])
  end

  # POST /salebills
  # POST /salebills.json
  def create
    @salebill = Salebill.new(params[:salebill])

    respond_to do |format|
      if @salebill.save
        format.html { redirect_to "/salebills", notice: 'Salebill was successfully created.' }
        format.json { render json: @salebill, status: :created, location: @salebill }
      else
        format.html { render action: "new" }
        format.json { render json: @salebill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /salebills/1
  # PUT /salebills/1.json
  def update
    @salebill = Salebill.find(params[:id])

    respond_to do |format|
      if @salebill.update_attributes(params[:salebill])
        format.html { redirect_to "/salebills", notice: 'Salebill was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @salebill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /salebills/1
  # DELETE /salebills/1.json
  def destroy
    @salebill = Salebill.find(params[:id])
    @salebill.destroy

    respond_to do |format|
      format.html { redirect_to salebills_url }
      format.json { head :no_content }
    end
  end
end
