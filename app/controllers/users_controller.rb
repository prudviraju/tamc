class UsersController < ApplicationController
  # GET /users
  # GET /users.json
    include ApplicationHelper
    include UsersHelper
  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json {render json: @users}
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json {render json: @user}
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json {render json: @user}
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        format.html {redirect_to @user, notice: 'User was successfully created.'}
        format.json {render json: @user, status: :created, location: @user}
      else
        format.html {render action: "new"}
        format.json {render json: @user.errors, status: :unprocessable_entity}
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html {redirect_to @user, notice: 'User was successfully updated.'}
        format.json {head :no_content}
      else
        format.html {render action: "edit"}
        format.json {render json: @user.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html {redirect_to users_url}
      format.json {head :no_content}
    end
  end

  def create_user
    @user = User.find_by_id(params[:user_id]) if params[:user_id].present?
  end

  def update_user
    @user = User.find_by_id(params[:user_id]) if params[:user_id].present?
    @user = User.new if @user.blank?
    @user.name = params[:name]
    @user.mobile_no = params[:mobile_no]
    @user.amc_id = params[:amc_id]
    @user.email = params[:email]
    @user.is_admin = true
    @user.password1 = "AMC12345"
    @user.password = "AMC12345"
    @user.password_confirmation = "AMC12345"
    @user.save!

    redirect_to '/users'
  end


  def create_agent
    @user = User.find_by_id(params[:agent_id]) if params[:agent_id].present?
  end

  def update_agent
    @user = User.find_by_id(params[:user_id]) if params[:user_id].present?
    @user = User.new if @user.blank?
    @user.registred_level = params[:registred_level] if params[:registred_level].present?
    @user.name = params[:name] if params[:name].present?
    @user.state ="TELANGANA"
    @user.district = params[:district] if params[:district].present?
    @user.email = params[:name].gsub(/\s/, '')+ rand.to_s[1..2]+"@tam.com"
    @user.company_name = params[:company_name] if params[:company_name].present?
    @user.bank_name = params[:bank_name] if params[:bank_name].present?
    @user.agent_type = params[:agent_type] if params[:agent_type].present?
    @user.user_role = params[:agent_type] if params[:agent_type].present?
    @user.gender = params[:gender] if params[:gender].present?
    @user.mandal = params[:mandal] if params[:mandal].present?
    @user.village = params[:village] if params[:village].present?
    @user.pan_card_no = params[:pan_card_no] if params[:pan_card_no].present?
    @user.service_provider = params[:service_provider] if params[:service_provider].present?
    @user.category = params[:segment_id] if params[:segment_id].present?
    @user.locality = params[:locality] if params[:locality].present?
    @user.pincode = params[:pincode] if params[:pincode].present?
    @user.aadhar_no = params[:aadhar_no] if params[:aadhar_no].present?
    @user.trade_limit = params[:trade_limit] if params[:trade_limit].present?
    @user.ifsc_code = params[:ifsc_code] if params[:ifsc_code].present?
    @user.remarks = params[:remarks] if params[:remarks].present?
    @user.mobile_no = params[:mobile_no] if params[:mobile_no].present?
    @user.account_number = params[:account_number] if params[:account_number].present?
    @user.apmc_licence_no = params[:apmc_licence_no] if params[:apmc_licence_no].present?
    @user.company_reg_no = params[:company_reg_no] if params[:company_reg_no].present?
    @user.amc_id = current_user.amc_id
    @user.password1 = params[:mobile_no]
    @user.password = params[:mobile_no]
    @user.password_confirmation = params[:mobile_no]
    @user.save!
    redirect_to '/users/agents_list'
  end

  def agents_list

    @limit = !params[:limit].blank? ? params[:limit] : 30
    @offset = !params[:offset].blank? ? params[:offset] : 0
    @userscount = User.name_scope(params[:name]).mobile_number_scope(params[:mobile_number]).company_name_scope(params[:company_name]).amc_scope(current_user.amc_id).admin_scope().employee_scope().size
    @users = User.name_scope(params[:name]).mobile_number_scope(params[:mobile_number]).company_name_scope(params[:company_name]).amc_scope(current_user.amc_id).admin_scope().employee_scope().limit(@limit).offset(@offset).reverse


    respond_to do |format|
      format.html # index.html.erb
      format.js
      format.json {render json: @users}
    end
  end

  def employee_list
    @users = User.find_all_by_amc_id_and_is_admin_and_is_employee(current_user.amc_id, false,true)
  end

  def create_employee
    @user = User.find_by_id(params[:user_id]) if params[:user_id].present?
  end

  def update_employee
      @user = User.find_by_id(params[:user_id]) if params[:user_id].present?
      @user = User.new if @user.blank?
      @user.name = params[:name]
      @user.mobile_no = params[:mobile_no]
      @user.user_role = params[:user_role]
      @user.amc_id = current_user.amc_id
      @user.email = params[:email]
      @user.user_code = params[:user_code]
      @user.segment_id = params[:segment_id]
      @user.is_employee = true
      @user.password1 = params[:password1]
      @user.password = params[:password1]
      @user.password_confirmation = params[:password1]
      @user.save!
      redirect_to '/users/employee_list'
  end


    def user_verification

      @details = {}
      @listLogin = []
      @user = User.find_by_user_code_and_password1(params[:loginId], params[:password])
      if @user.present?
        @amc = Amc.find_by_id(@user.amc_id)
        @user_role = UserRole.find_by_role_name(@user.user_role)
        @state = State.find_by_id(@amc.state_id)
        @listLogin << UserInformation.new(@amc.id, @state.id, @user.user_code, @user.name, @user_role.user_type, @user.id, @user_role.user_type, '', 'S', @state.id, @state.name, @user.mobile_no, @user.email, @user.name, @amc.name)
        @details = FinalUserInformation.new('S', @listLogin)
      end
      respond_to do |format|
        if @user.present?
          format.json {render json: @details}
        else
          format.json {render :json => {:error_message => "NOT AVAILABLE"}}
        end
      end
    end


  def agentList
    @users = User.where('amc_id=?  and is_admin =? and is_employee =? and user_role =? ',params[:oprId],false ,false ,'CommissionAgent')
    @gateEntryCommissionAgentList = []
    if @users.present?
      @users.each do |user|
        @gateEntryCommissionAgentList << AgentInformation.new(user.id, user.name,user.mobile_no ,user.company_name,user.user_role,user.apmc_licence_no,user.pan_card_no,'',"S",'')
      end
      @details = FinalAgentInformation.new('S', @gateEntryCommissionAgentList)
    end
    respond_to do |format|
      if @users.present?
        format.json {render json: @details}
      else
        format.json {render :json => {:error_message => "NOT AVAILABLE"}}
      end
    end
  end

    def getMasterDataHW
      @users = User.where('amc_id=?  and is_admin =? and is_employee =?',params[:oprId],false ,true)
      @getMasterDataHW = []
      if @users.present?
        @users.each do |user|
          @getMasterDataHW << HwInformation.new(user.id, user.name,user.user_role)
        end
        @details = FinalAgentInformation.new('S', @getMasterDataHW)
      end
      respond_to do |format|
        if @users.present?
          format.json {render json: @details}
        else
          format.json {render :json => {:error_message => "NOT AVAILABLE"}}
        end
      end
    end


  def cold_storages_update
    @cold_storages = ColdStorage.find_all_by_amc_id(1)
    @cold_storages.each do |cold|
      @cold_storage = ColdStorage.new
      @cold_storage.name = cold.name
      @cold_storage.active = true
      @cold_storage.amc_id = 6
      @cold_storage.save
    end
    respond_to do |format|
        format.json {render :json => {:error_message => "UPDATED"}}
    end
  end


    def getCaNames
      ca_names = []
      users = User.getAllCaList(params[:term],current_user.amc_id)
      users.each do |user|
        ca_names << user.company_name.to_s+"-" + user.id.to_s
      end
      render json: ca_names
    end


    def inactive_user
      @user = User.find_by_id(params[:user_id]) if params[:user_id].present?
      @user.update_attribute(:is_active, false ) if @user.present?
      redirect_to '/users/agents_list'
    end

    def active_user
      @user = User.find_by_id(params[:user_id]) if params[:user_id].present?
      @user.update_attribute(:is_active, true ) if @user.present?
      redirect_to '/users/agents_list'
    end
end
