class MarketChargesController < ApplicationController
  # GET /market_charges
  # GET /market_charges.json
  include ApplicationHelper
  def index
    if current_user.present? && current_user.amc_id.present?
      @market_charges = MarketCharge.where("amc_id=?",current_user.amc_id)
    else
      @market_charges = MarketCharge.all
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @market_charges }
    end
  end

  # GET /market_charges/1
  # GET /market_charges/1.json
  def show
    @market_charge = MarketCharge.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @market_charge }
    end
  end

  # GET /market_charges/new
  # GET /market_charges/new.json
  def new
    @market_charge = MarketCharge.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @market_charge }
    end
  end

  # GET /market_charges/1/edit
  def edit
    @market_charge = MarketCharge.find(params[:id])
  end

  # POST /market_charges
  # POST /market_charges.json
  def create
    @market_charge = MarketCharge.new(params[:market_charge])

    if params[:market_charge][:attachement].present?
      uploaded_io = params[:market_charge][:attachement]
      file_path_name = UUIDTools::UUID.random_create.to_s.delete '-'
      just_filename = File.basename(uploaded_io.original_filename)
      file_path_name += just_filename.sub(/[^\w\.\-]/, '_')
      File.open(Rails.root.join('public', 'uploads', file_path_name), 'w+b') do |file|
        file.write(uploaded_io.read)
      end
      @market_charge.attachement = ApplicationHelper.get_root_url+'uploads/'+file_path_name
    end
      @market_charge.modified_on = Date.today

    respond_to do |format|
      if @market_charge.save
        format.html { redirect_to '/market_charges', notice: 'Market charge was successfully created.' }
        format.json { render json: @market_charge, status: :created, location: @market_charge }
      else
        format.html { render action: "new" }
        format.json { render json: @market_charge.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /market_charges/1
  # PUT /market_charges/1.json
  def update
    @market_charge = MarketCharge.find(params[:id])
    @market_charge.modified_on = Date.today
    if params[:market_charge][:attachement].present?
      uploaded_io = params[:market_charge][:attachement]
      file_path_name = UUIDTools::UUID.random_create.to_s.delete '-'
      just_filename = File.basename(uploaded_io.original_filename)
      file_path_name += just_filename.sub(/[^\w\.\-]/, '_')
      File.open(Rails.root.join('public', 'uploads', file_path_name), 'w+b') do |file|
        file.write(uploaded_io.read)
      end
      @market_charge.attachement = ApplicationHelper.get_root_url+'uploads/'+file_path_name
    end
    respond_to do |format|
      if @market_charge.update_attributes(params[:market_charge])
        format.html { redirect_to '/market_charges', notice: 'Market charge was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @market_charge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /market_charges/1
  # DELETE /market_charges/1.json
  def destroy
    @market_charge = MarketCharge.find(params[:id])
    @market_charge.destroy

    respond_to do |format|
      format.html { redirect_to market_charges_url }
      format.json { head :no_content }
    end
  end
end
