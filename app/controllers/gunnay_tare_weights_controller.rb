class GunnayTareWeightsController < ApplicationController
  # GET /gunnay_tare_weights
  # GET /gunnay_tare_weights.json
  def index
    @gunnay_tare_weights = GunnayTareWeight.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @gunnay_tare_weights }
    end
  end

  # GET /gunnay_tare_weights/1
  # GET /gunnay_tare_weights/1.json
  def show
    @gunnay_tare_weight = GunnayTareWeight.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gunnay_tare_weight }
    end
  end

  # GET /gunnay_tare_weights/new
  # GET /gunnay_tare_weights/new.json
  def new
    @gunnay_tare_weight = GunnayTareWeight.new
    @gunny_detail = GunnyDetail.find_by_id(params[:gunny_id])
    @gunnay_tare_weight_list = GunnayTareWeight.where('gunny_id=?',@gunny_detail.id)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @gunnay_tare_weight }
    end
  end

  # GET /gunnay_tare_weights/1/edit
  def edit
    @gunnay_tare_weight = GunnayTareWeight.find(params[:id])
    @gunny_detail = GunnyDetail.find_by_id(@gunnay_tare_weight.gunny_id)
    @gunnay_tare_weight_list = GunnayTareWeight.where('gunny_id=?',@gunny_detail.id)
  end

  # POST /gunnay_tare_weights
  # POST /gunnay_tare_weights.json
  def create
    @gunnay_tare_weight = GunnayTareWeight.new(params[:gunnay_tare_weight])

    respond_to do |format|
      if @gunnay_tare_weight.save
        format.html { redirect_to "/gunnay_tare_weights/new?gunny_id=#{@gunnay_tare_weight.gunny_id}", notice: 'Gunnay tare weight was successfully created.' }
        format.json { render json: @gunnay_tare_weight, status: :created, location: @gunnay_tare_weight }
      else
        format.html { render action: "new" }
        format.json { render json: @gunnay_tare_weight.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /gunnay_tare_weights/1
  # PUT /gunnay_tare_weights/1.json
  def update
    @gunnay_tare_weight = GunnayTareWeight.find(params[:id])

    respond_to do |format|
      if @gunnay_tare_weight.update_attributes(params[:gunnay_tare_weight])
        format.html { redirect_to "/gunnay_tare_weights/new?gunny_id=#{@gunnay_tare_weight.gunny_id}", notice: 'Gunnay tare weight was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gunnay_tare_weight.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gunnay_tare_weights/1
  # DELETE /gunnay_tare_weights/1.json
  def destroy
    @gunnay_tare_weight = GunnayTareWeight.find(params[:id])
    @gunnay_tare_weight.destroy

    respond_to do |format|
      format.html { redirect_to gunnay_tare_weights_url }
      format.json { head :no_content }
    end
  end
end
