class FarmersController < ApplicationController
  # GET /farmers
  # GET /farmers.json
  include FarmersHelper
  include StatesHelper
  include ApplicationHelper
  def index
    if current_user.present? && current_user.amc_id.present?
      @farmers = Farmer.where("amc_id=?",current_user.amc_id).reverse
    else
      @farmers = Farmer.all
    end


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @farmers }
    end
  end

  # GET /farmers/1
  # GET /farmers/1.json
  def show
    @farmer = Farmer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @farmer }
    end
  end

  # GET /farmers/new
  # GET /farmers/new.json
  def new
    @farmer = Farmer.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @farmer }
    end
  end

  # GET /farmers/1/edit
  def edit
    @farmer = Farmer.find(params[:id])
  end

  # POST /farmers
  # POST /farmers.json
  def create
    @farmer = Farmer.new(params[:farmer])

    respond_to do |format|
      if @farmer.save
        format.html { redirect_to '/farmers', notice: 'Farmer was successfully created.' }
        format.json { render json: @farmer, status: :created, location: @farmer }
      else
        format.html { render action: "new" }
        format.json { render json: @farmer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /farmers/1
  # PUT /farmers/1.json
  def update
    @farmer = Farmer.find(params[:id])

    respond_to do |format|
      if @farmer.update_attributes(params[:farmer])
        format.html { redirect_to '/farmers', notice: 'Farmer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @farmer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /farmers/1
  # DELETE /farmers/1.json
  def destroy
    @farmer = Farmer.find(params[:id])
    @farmer.destroy

    respond_to do |format|
      format.html { redirect_to farmers_url }
      format.json { head :no_content }
    end
  end


  def stateList
    @states = State.all
    @stateList = []
    if @states.present?
      @states.each do |state|
        @stateList << StatesInformation.new('S', state.id, state.name)
      end
      @details = FinalStatesInformation.new('S', @stateList)
    end
    respond_to do |format|
      format.json {render json: @details}
    end
  end

  def districtsList
    @districts = District.all
    @districtList = []
    if @districts.present?
      @districts.each do |district|
        @districtList << DistrictInformation.new('S', district.state_id, district.id,district.name,)
      end
      @details = FinalDistrictInformation.new('S', @districtList)
    end
    respond_to do |format|
      format.json {render json: @details}
    end
  end

  def mandalsList
    @mandals = Mandal.all
    @tahsilList = []
    if @mandals.present?
      @mandals.each do |mandal|
        @tahsilList << MandInformation.new('S', mandal.state_id,mandal.district_id,mandal.id,mandal.name)
      end
      @details = FinalmandalInformation.new('S', @tahsilList)
    end
    respond_to do |format|
      format.json {render json: @details}
    end
  end

  def CityList
    @villages = Village.all
    @cityList = []
    if @villages.present?
      @villages.each do |village|
        @cityList << CityInformation.new('S', village.state_id,village.district_id,village.mandal_id,village.id,village.name)
      end
      @details = FinalCityInformation.new('S', @cityList)
    end
    respond_to do |format|
      format.json {render json: @details}
    end
  end


  def list_of_vehicle_types
    @vehicle_types = VehicleType.where("amc_id =?", params[:oprId]) if params[:oprId].present?
    @listVehicleType = []
    if @vehicle_types.present?
      @vehicle_types.each do |v_type|
        @listVehicleType << VechileTypesInformation.new('S', v_type.id, v_type.name)
      end
      @details = FinalVechileTypesInformation.new('S', @listVehicleType)
    end

    respond_to do |format|
      if if params[:oprId].present?
           format.json {render json: @details}
         else
           format.json {render :json => {:error_message => "NOT AVAILABLE"}}
         end
      end
    end
  end





  def list_of_uoms
    @uoms = UnitOfMeasurement.where("amc_id =?", params[:oprId]) if params[:oprId].present?
    respond_to do |format|
      if if params[:amc_id].present?
           format.json {render :json => {:Uoms => @uoms}}
         else
           format.json {render :json => {:error_message => "NOT AVAILABLE"}}
         end
      end
    end
  end

  def list_of_bag_types

    @bagtypes = BagType.where("amc_id =?", params[:oprId]) if params[:oprId].present?

    @listBagType = []
    if @bagtypes.present?
      @bagtypes.each do |bag_type|
        @listBagType << BagTypesInformation.new('S', bag_type.id, bag_type.description,bag_type.quandity ,bag_type.uom_id,'')
      end
      @details = FinalBagTypesInformation.new('S', @listBagType)
    end

    respond_to do |format|
      if if params[:oprId].present?
           format.json {render json: @details}
         else
           format.json {render :json => {:error_message => "NOT AVAILABLE"}}
         end
      end
    end
  end

  def list_of_commodities
    @commodities = Commodity.where("amc_id =?", params[:oprId]) if params[:oprId].present?
    @gateEntryCommodityList = []
    if @commodities.present?
      @commodities.each do |com|
        @gateEntryCommodityList << CommodityInformation.new(com.id,com.name, com.bag_type_id,com.uom_id,'',UnitOfMeasurement.find_by_id(com.uom_id).try(:unit_name),"S", '')
      end

      @details = FinalCommodityInformation.new('S', @gateEntryCommodityList)
    end

    respond_to do |format|
      if if params[:oprId].present?
           format.json {render json: @details}
         else
           format.json {render :json => {:error_message => "NOT AVAILABLE"}}
         end
      end
    end
  end


  def list_of_farmer
   @farmers = Farmer.where('amc_id=? and name like? or contact_no like? ',params[:oprId],'%'+params[:searchText]+'%','%'+params[:searchText]+'%')
   @farmerList = []
  if @farmers.present?
    @farmers.each do |farmer|
      @farmerList<< FarmerInformation.new(farmer.id, farmer.name,farmer.contact_no,farmer.state,farmer.district,farmer.mandal,farmer.village,farmer.proof_id,farmer.id_Proff_type,farmer.service_provider,"","","S","")
    end
    @details = FinalFarmerInformation.new('S', @farmerList)
  end
    respond_to do |format|
      if if params[:oprId].present?
           format.json {render json: @details}
         else
           format.json {render :json => {:error_message => "NOT AVAILABLE"}}
         end
      end
    end
  end


  def create_new_farmer
    @farmer = Farmer.new
    @farmer.name = params[:name]
    @farmer.relative_name = params[:relative_name]  if params[:relative_name].present?
    @farmer.relation = params[:relation] if params[:relation].present?
    @farmer.mandal = params[:mandal]
    @farmer.contact_no = params[:contact_no]
    @farmer.village = params[:village]
    @farmer.state = params[:state]
    @farmer.district = params[:district]
    @farmer.amc_id = current_user.amc_id
    @farmer.save
    session[SessionConstants::LAST_CREATED_FARMER] = @farmer.id
    redirect_to '/gate_entries/new'
  end


  def dashboard
    @selected_day = params[:posted_on].present? ? params[:posted_on] : Date.today

    if current_user.is_super_admin && current_user.amc_id.present?

      @commodities = Commodity.where('amc_id =?', current_user.amc_id)
      @arrived_lots = Lot.where('amc_id=?', current_user.amc_id).size()
      @InProcess = Lot.where('amc_id=? and is_takpatti=? and is_gate_entry=? and is_active=?', current_user.amc_id, false, true, true).size()
      @completed = Lot.where('amc_id=? and is_takpatti=? and is_active=? ', current_user.amc_id, true, true).size()
      @Returned = Lot.where('amc_id=? and is_gate_entry=? and is_active=? ', current_user.amc_id, true, false).size()


      @today_arrivals = Lot.where('amc_id=? and posted_on=?', current_user.amc_id, @selected_day).size()
      @today_InProcess = Lot.where('amc_id=? and is_takpatti=? and is_gate_entry=? and is_active=? and posted_on=?', current_user.amc_id, false, true, true, @selected_day).size()
      @today_completed = Lot.where('amc_id=? and is_takpatti=? and is_active=? and posted_on=?', current_user.amc_id, true, true, @selected_day).size()
      @today_Returned = Lot.where('amc_id=? and is_gate_entry=? and is_active=? and posted_on=?', current_user.amc_id, true, false, @selected_day).size()

    elsif current_user.is_super_admin && current_user.amc_id.blank?
      @commodities = Commodity.all
      @arrived_lots = Lot.all.size()
      @InProcess = Lot.where('is_takpatti=? and is_gate_entry=? and is_active=?', false, true, true).size()
      @completed = Lot.where('is_takpatti=? and is_active=? ', true, true).size()
      @Returned = Lot.where('is_gate_entry=? and is_active=? ', true, false).size()

      @today_arrivals = Lot.where('posted_on=?', @selected_day).size()
      @today_InProcess = Lot.where('is_takpatti=? and is_gate_entry=? and is_active=? and posted_on=?', false, true, true, @selected_day).size()
      @today_completed = Lot.where('is_takpatti=? and is_active=? and posted_on=?', true, true, @selected_day).size()
      @today_Returned = Lot.where('is_gate_entry=? and is_active=? and posted_on=?', true, false, @selected_day).size()

    elsif current_user.is_admin || current_user.amc_id.present?
      @commodities = Commodity.where('amc_id =?', current_user.amc_id)
      @arrived_lots = Lot.where('amc_id=?', current_user.amc_id).size()
      @InProcess = Lot.where('amc_id=? and is_takpatti=? and is_gate_entry=? and is_active=?', current_user.amc_id, false, true, true).size()
      @completed = Lot.where('amc_id=? and is_takpatti=? and is_active=? ', current_user.amc_id, true, true).size()
      @Returned = Lot.where('amc_id=? and is_gate_entry=? and is_active=? ', current_user.amc_id, true, false).size()


      @today_arrivals = Lot.where('amc_id=? and posted_on=?', current_user.amc_id, @selected_day).size()
      @today_InProcess = Lot.where('amc_id=? and is_takpatti=? and is_gate_entry=? and is_active=? and posted_on=?', current_user.amc_id, false, true, true, @selected_day).size()
      @today_completed = Lot.where('amc_id=? and is_takpatti=? and is_active=? and posted_on=?', current_user.amc_id, true, true, @selected_day).size()
      @today_Returned = Lot.where('amc_id=? and is_gate_entry=? and is_active=? and posted_on=?', current_user.amc_id, true, false, @selected_day).size()


    end

    respond_to do |format|
      format.html
      format.js
    end

  end


  def dashboard1
    @selected_day = params[:posted_on].present? ? params[:posted_on] : Date.today
    @commodity_data = {}

    if current_user.present? && current_user.is_super_admin && current_user.amc_id.present?
      @commodities = Commodity.all
      @arrived_lots = ActiveRecord::Base.connection.execute("select count(*) from lots").first[0]
      @InProcess = ActiveRecord::Base.connection.execute("select count(*) from lots where is_takpatti=false and is_gate_entry=true and is_active=true").first[0]
      @completed = ActiveRecord::Base.connection.execute("select count(*) from lots where is_takpatti=true and is_gate_entry=true and is_active=true").first[0]
      @Returned = ActiveRecord::Base.connection.execute("select count(*) from lots where is_active=false").first[0]

      @today_arrivals = ActiveRecord::Base.connection.execute("select count(*) from lots where posted_on=#{@selected_day}").first[0]
      @today_InProcess = ActiveRecord::Base.connection.execute("select count(*) from lots where is_takpatti=false and is_gate_entry=true and is_active=true and posted_on=#{@selected_day}").first[0]
      @today_completed = ActiveRecord::Base.connection.execute("select count(*) from lots where is_takpatti=true and is_gate_entry=true and is_active=true and posted_on=#{@selected_day}").first[0]
      @today_Returned = ActiveRecord::Base.connection.execute("select count(*) from lots where is_active=false and posted_on=#{@selected_day}").first[0]

    elsif current_user.present? && current_user.amc_id.present?

      @arrived_lots = ActiveRecord::Base.connection.execute("select count(*) from lots where amc_id ='#{current_user.amc_id}'").first[0]
      @InProcess = ActiveRecord::Base.connection.execute("select count(*) from lots where amc_id = '#{current_user.amc_id}' and is_takpatti=false and is_gate_entry=true and is_active=true").first[0]
      @completed = ActiveRecord::Base.connection.execute("select count(*) from lots where amc_id = '#{current_user.amc_id}' and is_takpatti=true and is_gate_entry=true and is_active=true").first[0]
      @Returned = ActiveRecord::Base.connection.execute("select count(*) from lots where amc_id = '#{current_user.amc_id}' and is_active=false").first[0]

      @today_arrivals = ActiveRecord::Base.connection.execute("select count(*) from lots where amc_id ='#{current_user.amc_id}' and posted_on='#{@selected_day}'").first[0]
      @today_InProcess = ActiveRecord::Base.connection.execute("select count(*) from lots where amc_id ='#{current_user.amc_id}' and is_takpatti=false and is_gate_entry=true and is_active=true and posted_on='#{@selected_day}'").first[0]
      @today_completed = ActiveRecord::Base.connection.execute("select count(*) from lots where amc_id ='#{current_user.amc_id}' and is_takpatti=true and is_gate_entry=true and is_active=true and posted_on='#{@selected_day}'").first[0]
      @today_Returned = ActiveRecord::Base.connection.execute("select count(*) from lots where amc_id ='#{current_user.amc_id}' and is_active=false and posted_on='#{@selected_day}'").first[0]


      @commodity_data = ActiveRecord::Base.connection.execute("select  l.commodity_id as id ,c.name as name , count(l.id) as lot_count, sum(l.no_of_bags) as no_of_bags,sum(l.finalised_bag_count) as no_of_bags1
, max(case when l.bid_amount =0 then sl.max else l.bid_amount  end ) as lot_max
, min(case when l.bid_amount =0 then sl.min else l.bid_amount  end ) as lot_min
from TAMC_production.lots l
inner join TAMC_production.commodities c on c.id=l.commodity_id
left join(select lot_id , min(sl.bid_amount) as min , max(sl.bid_amount) as max from  TAMC_production.sub_lots sl group by lot_id) sl on sl.lot_id=l.id
where  l.posted_on ='#{@selected_day}' and l.amc_id ='#{current_user.amc_id}'group by l.commodity_id")
  end


    respond_to do |format|
      format.html
      format.js
    end

  end

  def amcwise_farmer_list
    farmers = Farmer.get_farmers(params[:term],current_user.amc_id)
    farmers_names = []
    farmers.each do |user|
      farmers_names << user.name+"--"+ user.contact_no
    end
    render json: farmers_names
  end

end
