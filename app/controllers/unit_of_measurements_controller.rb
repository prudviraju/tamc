class UnitOfMeasurementsController < ApplicationController
  # GET /unit_of_measurements
  # GET /unit_of_measurements.json
  def index
    if current_user.present? && current_user.amc_id.present?
      @unit_of_measurements = UnitOfMeasurement.where("amc_id =?",current_user.amc_id)
    else
      @unit_of_measurements = UnitOfMeasurement.all
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @unit_of_measurements }
    end
  end

  # GET /unit_of_measurements/1
  # GET /unit_of_measurements/1.json
  def show
    @unit_of_measurement = UnitOfMeasurement.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @unit_of_measurement }
    end
  end

  # GET /unit_of_measurements/new
  # GET /unit_of_measurements/new.json
  def new
    @unit_of_measurement = UnitOfMeasurement.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @unit_of_measurement }
    end
  end

  # GET /unit_of_measurements/1/edit
  def edit
    @unit_of_measurement = UnitOfMeasurement.find(params[:id])
  end

  # POST /unit_of_measurements
  # POST /unit_of_measurements.json
  def create
    @unit_of_measurement = UnitOfMeasurement.new(params[:unit_of_measurement])

    respond_to do |format|
      if @unit_of_measurement.save
        format.html { redirect_to '/unit_of_measurements', notice: 'Unit of measurement was successfully created.' }
        format.json { render json: @unit_of_measurement, status: :created, location: @unit_of_measurement }
      else
        format.html { render action: "new" }
        format.json { render json: @unit_of_measurement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /unit_of_measurements/1
  # PUT /unit_of_measurements/1.json
  def update
    @unit_of_measurement = UnitOfMeasurement.find(params[:id])

    respond_to do |format|
      if @unit_of_measurement.update_attributes(params[:unit_of_measurement])
        format.html { redirect_to '/unit_of_measurements', notice: 'Unit of measurement was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @unit_of_measurement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /unit_of_measurements/1
  # DELETE /unit_of_measurements/1.json
  def destroy
    @unit_of_measurement = UnitOfMeasurement.find(params[:id])
    @unit_of_measurement.destroy

    respond_to do |format|
      format.html { redirect_to unit_of_measurements_url }
      format.json { head :no_content }
    end
  end
end
