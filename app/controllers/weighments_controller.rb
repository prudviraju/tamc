class WeighmentsController < ApplicationController
  # GET /weighments
  # GET /weighments.json
  def index
    @weighments = Weighment.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @weighments }
    end
  end

  # GET /weighments/1
  # GET /weighments/1.json
  def show
    @weighment = Weighment.find(params[:id])
    @bag_weights = BagWeight.find_all_by_weighment_id(@weighment.id)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @weighment }
    end
  end

  # GET /weighments/new
  # GET /weighments/new.json
  def new
    @weighment = Weighment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @weighment }
    end
  end

  # GET /weighments/1/edit
  def edit
    @weighment = Weighment.find(params[:id])
  end

  # POST /weighments
  # POST /weighments.json
  def create
    @weighment = Weighment.new(params[:weighment])

    respond_to do |format|
      if @weighment.save
        format.html { redirect_to '/weighments', notice: 'Weighment was successfully created.' }
        format.json { render json: @weighment, status: :created, location: @weighment }
      else
        format.html { render action: "new" }
        format.json { render json: @weighment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /weighments/1
  # PUT /weighments/1.json
  def update
    @weighment = Weighment.find(params[:id])

    respond_to do |format|
      if @weighment.update_attributes(params[:weighment])
        format.html { redirect_to '/weighments', notice: 'Weighment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @weighment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /weighments/1
  # DELETE /weighments/1.json
  def destroy
    @weighment = Weighment.find(params[:id])
    @weighment.destroy

    respond_to do |format|
      format.html { redirect_to weighments_url }
      format.json { head :no_content }
    end
  end

  def get_lot_details
    
    @lot = Lot.find_by_lot_code(params[:lot_no])
  end


  def create_weighment

  end


  def update_weighment
   @weighment = Weighment.new
   @weighment.lot_code = params[:lot_code]
   @weighment.lot_id = Lot.find_by_lot_code(params[:lot_code]).try(:id)
   @weighment.seller_type = params[:seller_type]
   @weighment.bag_type = params[:bag_type]
   @weighment.seller_name = params[:seller_name]
   @weighment.product_id = params[:product_id]
   @weighment.no_of_bags = params[:no_of_bags]
   @weighment.lot_weight = params[:lot_weight]
   @weighment.remarks = params[:remarks]
   @weighment.amc_id = current_user.amc_id
   @weighment.save


   params.keys.each do |key|
     if key.starts_with?('sequence_')
       split_keys = key.split('_')
       index = key.split('_')[1]
       @bag_weight= BagWeight.new
       @bag_weight.weighment_id = @weighment.id
       @bag_weight.bagTypeId = @weighment.bag_type.to_i
       @bag_weight.lot_id = @weighment.lot_id
       @bag_weight.sequence = params["sequence_#{index}"]
       @bag_weight.netWeight = params["netWeight_#{index}"]
       @bag_weight.grossWeight = params["grossWeight_#{index}"]
       @bag_weight.tareWeight = params["tareWeight_#{index}"]
       @bag_weight.price = params["price_#{index}"]
       @bag_weight.save
     end
   end

    redirect_to '/weighments'
  end

  def edit_weighment
     @weighment = Weighment.find_by_id(params[:weighment_id])
     @bag_weights = BagWeight.find_all_by_weighment_id(@weighment.id)
  end


  def updated_weighment

    @weighment = Weighment.find_by_id(params[:weighment_id])
    @weighment.lot_code = params[:lot_code]
    @weighment.lot_id = Lot.find_by_lot_code(params[:lot_code]).try(:id)
    @weighment.seller_type = params[:seller_type]
    @weighment.bag_type = params[:bag_type]
    @weighment.seller_name = params[:seller_name]
    @weighment.product_id = params[:product_id]
    @weighment.no_of_bags = params[:no_of_bags]
    @weighment.lot_weight = params[:lot_weight]
    @weighment.remarks = params[:remarks]
    @weighment.amc_id = current_user.amc_id
    @weighment.save

    params.keys.each do |key|
      if key.starts_with?('sequence_')
        split_keys = key.split('_')
        index = key.split('_')[1]
        @bag_weight= BagWeight.find_by_weighment_id_and_sequence(@weighment.id,params["sequence_#{index}"])
        @bag_weight= BagWeight.new if @bag_weight.blank?
        @bag_weight.weighment_id = @weighment.id
        @bag_weight.bagTypeId = @weighment.bag_type.to_i
        @bag_weight.lot_id = @weighment.lot_id
        @bag_weight.netWeight = params["netWeight_#{index}"]
        @bag_weight.grossWeight = params["grossWeight_#{index}"]
        @bag_weight.tareWeight = params["tareWeight_#{index}"]
        @bag_weight.price = params["price_#{index}"]
        @bag_weight.save
      end
    end

    redirect_to '/weighments'
  end

end
