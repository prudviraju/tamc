TAMC::Application.routes.draw do
  resources :system_settings do
    get :getAppVersion, :on => :collection
  end

  resources :cold_storages

  resources :gunnay_tare_weights

  resources :bag_names

  resources :sub_lots

  resources :market_charges

  resources :segments

  resources :gunny_details

  resources :sale_bill_components

  resources :salebills

  resources :bag_weights

  resources :weighment_bag_weights

  resources :bids

  resources :weighments do
    get :get_lot_details, :on => :collection
    get :create_weighment, :on => :collection
    post :update_weighment, :on => :collection
    get :edit_weighment, :on => :collection
    post :updated_weighment, :on => :collection
  end

  resources :lots do
    get :bidding_lots_list, :on => :collection
    get :add_bid, :on => :collection
    post :update_bid, :on => :collection
    get :close_bidding, :on => :collection
    get :show_entry_ticket, :on => :collection
    get :wayment_done_lots, :on => :collection
    get :make_inactive, :on => :collection
    get :takparty_receipt, :on => :collection
    get :update_data, :on => :collection
    get :modify_lot, :on => :collection
    get :modify_lot, :on => :collection
    post :lot_updated, :on => :collection
    get :lots_information, :on => :collection
    get :arrivedLotsInformation, :on => :collection
    get :pendingLotsInformation, :on => :collection
    get :returnedLotsInformation, :on => :collection
    get :day_wise_summary, :on => :collection
    get :day_wise_summary1, :on => :collection
    post :nrBidUpdate, :on => :collection
    post :nrBidUpdate1, :on => :collection
    get :nrlotBidding, :on => :collection
    get :nrlotBidding1, :on => :collection
    get :market_fee_details, :on => :collection
    get :makeDelete, :on => :collection
    get :add_remarks, :on => :collection
    post :save_remarks, :on => :collection
    get :detailsOfFee, :on => :collection
    get :getLotNumbers, :on => :collection
    get :getlotInformation, :on => :collection
  end

  resources :gate_entries do
    get :show_lots, :on => :collection
    get :create_gate_pass, :on => :collection
    post :generate_gate_entries, :on => :collection
    get :get_farmers, :on => :collection
    get :generate_gate_entry, :on => :collection
    get :set_farmer_details, :on => :collection
  end

  resources :farmers do
    post :list_of_vehicle_types, :on => :collection
    post :list_of_uoms, :on => :collection
    post :list_of_bag_types, :on => :collection
    post :list_of_commodities, :on => :collection
    post :list_of_farmer, :on => :collection

    post :stateList, :on => :collection
    post :districtsList, :on => :collection
    post :mandalsList, :on => :collection
    post :CityList, :on => :collection
    post :create_new_farmer, :on => :collection
    get :dashboard, :on => :collection
    get :dashboard1, :on => :collection
    get :amcwise_farmer_list, :on => :collection



  end

  resources :fee_components

  resources :fee_categories do
    get :listFeeCategoryForAutoSaleModel, :on => :collection
    get :listFeeComponentMstDtlAutoSaleModel, :on => :collection
  end

  resources :commodities

  resources :bag_types

  resources :unit_of_measurements

  resources :vehicle_types

  resources :users do
    get :create_user, :on => :collection
    post :update_user, :on => :collection
    get :agents_list, :on => :collection
    get :employee_list, :on => :collection
    get :create_agent, :on => :collection
    post :update_agent, :on => :collection
    get :create_employee, :on => :collection
    post :update_employee, :on => :collection
    post :user_verification, :on => :collection
    post :agentList, :on => :collection
    get :getMasterDataHW, :on => :collection
    get :cold_storages_update, :on => :collection
    get :getCaNames, :on => :collection
    get :inactive_user, :on => :collection
    get :active_user, :on => :collection
  end

  resources :user_roles

  resources :villages do
    get :mandal_wise_villages, :on => :collection
  end

  resources :mandals do
    get :districtwise_mandals_list, :on => :collection
  end

  resources :districts do
    get :statewise_district_list, :on => :collection
  end

  resources :states do


  end

  resources :amcs do
    get :all_amc_list, :on => :collection
    get :select_amc, :on => :collection
  end


  resources :user_sessions do
    get :new, :on => :collection
  end


  match 'logout', :controller => 'user_sessions', :action => 'destroy'
  match 'login', :controller => 'user_sessions', :action => 'new'


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'user_sessions#new'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
