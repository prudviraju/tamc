authorization do
  role :super_admin do
    includes :admin
    includes :co
    has_permission_on [:amcs], :to => [:new, :index, :show, :create, :edit, :update, :destroy,:all_amc_list,:select_amc]
    has_permission_on [:states], :to => [:new, :index, :show, :create, :edit, :update, :destroy]
    has_permission_on [:districts], :to => [:new, :index, :show, :create, :edit, :update, :destroy]
    has_permission_on [:mandals], :to => [:new, :index, :show, :create, :edit, :update, :destroy]
    has_permission_on [:villages], :to => [:new, :index, :show, :create, :edit, :update, :destroy]
    has_permission_on [:user_roles], :to => [:new, :index, :show, :create, :edit, :update, :destroy]
  end

  role :admin do
    includes :guest
    has_permission_on [:progress_records], :to => [:camp_level_progress]
  end

  role :guest do
    has_permission_on [:districts], :to => [:statewise_district_list]
    has_permission_on [:mandals], :to => [:districtwise_mandals_list]
    has_permission_on [:villages], :to => [:mandal_wise_villages]
  end
end




