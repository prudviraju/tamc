class CreateVehicleTypes < ActiveRecord::Migration
  def change
    create_table :vehicle_types do |t|
      t.string :name
      t.integer :amc_id
      t.text :description
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
