class CreateGunnyDetails < ActiveRecord::Migration
  def change
    create_table :gunny_details do |t|
      t.integer :commodity_id
      t.integer :bag_type_id
      t.float :tare_weight, :default => 0.0
      t.integer :uom_id
      t.boolean :is_active, :default => true
      t.integer :amc_id

      t.timestamps
    end
  end
end
