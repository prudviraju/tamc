class AddFarmerAmountToLots < ActiveRecord::Migration
  def change
    add_column :lots, :farmer_amount, :float, :default => 0.0
    add_column :lots, :trader_payable_amount, :float, :default => 0.0
  end
end
