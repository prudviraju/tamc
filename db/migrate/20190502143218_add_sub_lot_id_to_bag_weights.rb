class AddSubLotIdToBagWeights < ActiveRecord::Migration
  def change
    add_column :bag_weights, :sublot_id, :integer
  end
end
