class AddCreatedByToGateEntries < ActiveRecord::Migration
  def change
    add_column :gate_entries, :created_by, :integer
    add_column :gate_entries, :gate_entry_type, :string
    add_column :gate_entries, :device_id, :string
    add_column :gate_entries, :deviceType, :string
    add_column :gate_entries, :tempLot, :string, :default => "N"
    add_column :gate_entries, :trade_type, :string, :default => "N"
  end
end
