class CreateWeighments < ActiveRecord::Migration
  def change
    create_table :weighments do |t|
      t.string :weighment_type
      t.string :lot_code
      t.integer :lot_id
      t.string :seller_type
      t.integer :bag_type
      t.integer :product_id
      t.integer :seller_name
      t.integer :no_of_bags
      t.float :lot_weight, :default => 0.0
      t.float :lot_weight_in_no, :default => 0.0
      t.float :weighment_charges, :default => 0.0
      t.text :remarks
      t.integer :amc_id
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
