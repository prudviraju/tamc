class AddBidAmountToLots < ActiveRecord::Migration
  def change
    add_column :lots, :bid_amount, :float, :default => 0.0
    add_column :lots, :bid_raise_by, :integer
    add_column :lots, :posted_on, :date
    add_column :gate_entries, :posted_on, :date

  end
end
