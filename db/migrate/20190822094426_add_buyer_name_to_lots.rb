class AddBuyerNameToLots < ActiveRecord::Migration
  def change
    add_column :lots, :buyer_name, :string
  end
end
