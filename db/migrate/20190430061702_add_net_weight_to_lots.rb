class AddNetWeightToLots < ActiveRecord::Migration
  def change
    add_column :lots, :netWeight, :float, :default => 0.0
    add_column :lots, :grossWeight, :float, :default => 0.0
    add_column :lots, :tareWeight, :float, :default => 0.0
    add_column :lots, :gross_amount, :float, :default => 0.0
    add_column :lots, :net_payable, :float, :default => 0.0
    add_column :lots, :net_amount, :float, :default => 0.0
    add_column :lots, :rate_for_qui, :float, :default => 0.0
    add_column :lots, :finalised_bag_count, :integer
    add_column :lots, :transaction_no, :string
    add_column :lots, :invoice_doc_number, :string
  end
end
