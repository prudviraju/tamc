class CreateLotSequences < ActiveRecord::Migration
  def change
    create_table :lot_sequences do |t|
      t.integer :amc_id
      t.date :lot_date
      t.integer :sequence_num
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
