class CreateSegments < ActiveRecord::Migration
  def change
    create_table :segments do |t|
      t.integer :amc_id
      t.string :segment_name
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
