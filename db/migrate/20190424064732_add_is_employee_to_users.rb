class AddIsEmployeeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_employee, :boolean, :default => false
  end
end
