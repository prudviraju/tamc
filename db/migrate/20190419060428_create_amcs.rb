class CreateAmcs < ActiveRecord::Migration
  def change
    create_table :amcs do |t|
      t.string :name
      t.string :code
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
