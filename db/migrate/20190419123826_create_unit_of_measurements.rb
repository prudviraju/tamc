class CreateUnitOfMeasurements < ActiveRecord::Migration
  def change
    create_table :unit_of_measurements do |t|
      t.string :unit_name
      t.text :description
      t.integer :amc_id
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
