class AddCommodityIdToFeeCategories < ActiveRecord::Migration
  def change
    add_column :fee_categories, :commodity_id, :integer
  end
end
