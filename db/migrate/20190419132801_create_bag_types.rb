class CreateBagTypes < ActiveRecord::Migration
  def change
    create_table :bag_types do |t|
      t.integer :amc_id
      t.decimal :quandity
      t.integer :uom_id
      t.string :description
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
