class CreateSaleBillComponents < ActiveRecord::Migration
  def change
    create_table :sale_bill_components do |t|
      t.integer :component_id
      t.float :amount, :default => 0.0
      t.float :deducted_amount, :default => 0.0
      t.string :amount_calculated_type, :default => 0.0
      t.float :percent, :default => 0.0
      t.float :discount_percent, :default => 0.0
      t.string :party_type
      t.string :party_name
      t.integer :party_id
      t.integer :salebill_id

      t.timestamps
    end
  end
end
