class CreateMarketCharges < ActiveRecord::Migration
  def change
    create_table :market_charges do |t|
      t.string :name
      t.string :unit
      t.float :above_weight, :default => 0.0
      t.float :below_weight, :default => 0.0
      t.float :charge_value, :default => 0.0
      t.string :charge_type
      t.string :deduction_type
      t.integer :commodity_id
      t.integer :amc_id
      t.string :party_type
      t.integer :modified_by
      t.date :modified_on
      t.string :attachement
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
