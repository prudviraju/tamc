class CreateColdStorages < ActiveRecord::Migration
  def change
    create_table :cold_storages do |t|
      t.string :name
      t.boolean :active, :default => true
      t.integer :amc_id

      t.timestamps
    end
  end
end
