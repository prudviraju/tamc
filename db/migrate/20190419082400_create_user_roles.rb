class CreateUserRoles < ActiveRecord::Migration
  def change
    create_table :user_roles do |t|
      t.string :role_name
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
