class CreateBagNames < ActiveRecord::Migration
  def change
    create_table :bag_names do |t|
      t.string :name
      t.boolean :is_active, :default => true
      t.integer :amc_id

      t.timestamps
    end
  end
end
