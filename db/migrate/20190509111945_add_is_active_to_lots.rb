class AddIsActiveToLots < ActiveRecord::Migration
  def change
    add_column :lots, :is_active, :boolean, :default => true
  end
end
