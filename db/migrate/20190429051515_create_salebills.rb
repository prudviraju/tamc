class CreateSalebills < ActiveRecord::Migration
  def change
    create_table :salebills do |t|
      t.string :agreement_type
      t.string :seller_type
      t.integer :seller_id
      t.integer :trader_id
      t.string :firm_name
      t.integer :lot_id
      t.string :lot_code
      t.integer :commodity_id
      t.float :apx_weight, :default => 0.0
      t.float :rate_for_qui, :default => 0.0
      t.float :net_weight, :default => 0.0
      t.float :actual_weight, :default => 0.0
      t.integer :fee_category_id
      t.integer :bag_type_id
      t.integer :no_of_bags, :default => 0
      t.float :net_amount, :default => 0.0
      t.float :gross_amount, :default => 0.0
      t.float :net_payable, :default => 0.0
      t.integer :amc_id

      t.timestamps
    end
  end
end
