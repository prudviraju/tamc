class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :mobile_no
      t.string :aadhar_no
      t.string :door_no
      t.string :village
      t.string :mandal
      t.string :district
      t.string :pincode
      t.string :email
      t.string :crypted_password
      t.string :password_salt
      t.string :persistence_token
      t.string :perishable_token
      t.string :password
      t.string :password_confirmation
      t.boolean :is_admin, :default => false
      t.boolean :is_super_admin, :default => false
      t.string :user_role
      t.integer :amc_id
      t.string :password1
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
