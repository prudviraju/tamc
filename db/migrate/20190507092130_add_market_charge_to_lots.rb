class AddMarketChargeToLots < ActiveRecord::Migration
  def change
    add_column :lots, :market_charges, :float, :default => 0.0
    add_column :lots, :ca_charges, :float, :default => 0.0
    add_column :lots, :gunny_amount, :float, :default => 0.0
  end
end
