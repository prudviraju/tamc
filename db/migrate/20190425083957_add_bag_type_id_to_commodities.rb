class AddBagTypeIdToCommodities < ActiveRecord::Migration
  def change
    add_column :commodities, :bag_type_id, :integer
  end
end
