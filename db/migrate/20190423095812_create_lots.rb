class CreateLots < ActiveRecord::Migration
  def change
    create_table :lots do |t|
      t.string :seller_type
      t.integer :seller_id
      t.string :mobile_no
      t.string :state
      t.string :district
      t.string :mandal
      t.string :village
      t.string :commodity_id
      t.integer :bag_type_id
      t.integer :no_of_bags, :default => 0
      t.float :aprox_quantity, :default => 0.0
      t.integer :vehicle_type_id
      t.string :vechile_no
      t.string :ca_firm
      t.integer :ca_name
      t.integer :amc_id
      t.string :gate_entry_pass_no
      t.string :lot_code
      t.boolean :is_gate_entry, :default => false
      t.boolean :is_bid_rigistred, :default => false
      t.boolean :is_weighment, :default => false
      t.boolean :is_takpatti, :default => false

      t.timestamps
    end
  end
end
