class CreateFarmers < ActiveRecord::Migration
  def change
    create_table :farmers do |t|
      t.string :name
      t.string :gender
      t.date :date_of_birth
      t.date :registration_date
      t.string :registration_level
      t.string :service_provider
      t.string :category
      t.string :locality
      t.string :village
      t.string :mandal
      t.string :district
      t.string :state
      t.string :pin_code
      t.string :contact_no
      t.string :email
      t.string :id_Proff_type
      t.string :proof_id
      t.text :remarks
      t.string :relation
      t.string :relative_name
      t.string :bank_name
      t.string :account_number
      t.string :ifsc_code
      t.boolean :is_active, :default => true
      t.integer :amc_id

      t.timestamps
    end
  end
end
