class AddRemarksToLots < ActiveRecord::Migration
  def change
    add_column :lots, :remarks, :text
  end
end
