class CreateCommodities < ActiveRecord::Migration
  def change
    create_table :commodities do |t|
      t.string :name
      t.integer :uom_id
      t.integer :amc_id
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
