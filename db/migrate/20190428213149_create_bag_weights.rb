class CreateBagWeights < ActiveRecord::Migration
  def change
    create_table :bag_weights do |t|
      t.integer :lot_id
      t.integer :weighment_id
      t.integer :bagTypeId
      t.integer :sequence, :default => 0
      t.float :netWeight, :default => 0.0
      t.float :grossWeight, :default => 0.0
      t.float :tareWeight, :default => 0.0
      t.float :price, :default => 0.0
      t.string :grade

      t.timestamps
    end
  end
end
