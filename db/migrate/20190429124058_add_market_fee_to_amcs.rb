class AddMarketFeeToAmcs < ActiveRecord::Migration
  def change
    add_column :amcs, :market_fee, :float, :default => 0.0
  end
end
