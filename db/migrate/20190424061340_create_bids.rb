class CreateBids < ActiveRecord::Migration
  def change
    create_table :bids do |t|
      t.integer :lot_id
      t.integer :agent_id
      t.integer :seller_id
      t.float :bid_amount, :default => 0.0
      t.boolean :is_active, :default => true
      t.boolean :is_approved, :default => false

      t.timestamps
    end
  end
end
