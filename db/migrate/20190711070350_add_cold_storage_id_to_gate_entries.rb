class AddColdStorageIdToGateEntries < ActiveRecord::Migration
  def change
    add_column :gate_entries, :cold_storage_id, :integer
  end
end
