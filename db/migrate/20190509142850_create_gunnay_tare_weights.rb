class CreateGunnayTareWeights < ActiveRecord::Migration
  def change
    create_table :gunnay_tare_weights do |t|
      t.integer :gunny_id
      t.string :bag_name
      t.float :tare_weight, :default => 0.0
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
