class AddBuyerTypeToLots < ActiveRecord::Migration
  def change
    add_column :lots, :buyer_type, :string
  end
end
