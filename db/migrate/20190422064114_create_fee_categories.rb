class CreateFeeCategories < ActiveRecord::Migration
  def change
    create_table :fee_categories do |t|
      t.string :category_name
      t.integer :amc_id
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
