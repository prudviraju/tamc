class CreateFeeComponents < ActiveRecord::Migration
  def change
    create_table :fee_components do |t|
      t.string :name
      t.string :fee_type
      t.float :percentage, :default => 0.0
      t.float :amount, :default => 0.0
      t.string :party_type
      t.float :calculate_on_uom
      t.float :amount_per_uom, :default => 0.0
      t.float :tds, :default => 0.0
      t.integer :modified_by
      t.datetime :modified_on
      t.integer :amc_id
      t.integer :fee_category_id
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
