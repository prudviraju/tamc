class AddOldFarmerToFarmers < ActiveRecord::Migration
  def change
    add_column :farmers, :old_farmer, :boolean,:default => true
  end
end
