class CreateVillages < ActiveRecord::Migration
  def change
    create_table :villages do |t|
      t.string :name
      t.integer :state_id
      t.integer :district_id
      t.integer :mandal_id
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
