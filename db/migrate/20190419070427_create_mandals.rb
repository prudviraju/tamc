class CreateMandals < ActiveRecord::Migration
  def change
    create_table :mandals do |t|
      t.string :name
      t.string :code
      t.integer :state_id
      t.integer :district_id
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
