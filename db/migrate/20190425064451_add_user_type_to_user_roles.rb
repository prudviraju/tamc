class AddUserTypeToUserRoles < ActiveRecord::Migration
  def change
    add_column :user_roles, :user_type, :string
    add_column :user_roles, :user_code, :string
    add_column :users, :user_code, :string
  end
end
