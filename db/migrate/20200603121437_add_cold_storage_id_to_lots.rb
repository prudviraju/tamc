class AddColdStorageIdToLots < ActiveRecord::Migration
  def change
    add_column :lots, :coldstore_id, :integer
  end
end
