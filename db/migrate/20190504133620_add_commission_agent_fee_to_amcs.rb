class AddCommissionAgentFeeToAmcs < ActiveRecord::Migration
  def change
    add_column :amcs, :commission_agent_fee, :float, :default => 0.0
  end
end
