class CreateLotDeductions < ActiveRecord::Migration
  def change
    create_table :lot_deductions do |t|
      t.integer :bagSeqno
      t.string :lot_code
      t.integer :lot_id
      t.float :gross_Weight, :default => 0.0
      t.float :tare_weight, :default => 0.0
      t.float :net_weight, :default => 0.0
      t.float :bagamount, :default => 0.0
      t.float :deduction_amount, :default => 0.0
      t.string :party_type
      t.string :charge_type
      t.float :charge_value, :default => 0.0
      t.string :deduction_type
      t.boolean :is_active,:default => true

      t.timestamps
    end
  end
end
