class AddBagCostToCommodities < ActiveRecord::Migration
  def change
    add_column :commodities, :bag_cost, :float, :default => 0.0
  end
end
