class AddSegmentIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :segment_id, :integer
  end
end
