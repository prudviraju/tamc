class CreateSubLots < ActiveRecord::Migration
  def change
    create_table :sub_lots do |t|
      t.integer :lot_id
      t.float :bid_amount, :default => 0.0
      t.integer :no_of_bags
      t.float :grossWeight, :default => 0.0
      t.float :netWeight, :default => 0.0
      t.float :tareWeight, :default => 0.0
      t.integer :user_id
      t.string :device_id
      t.float :gross_amount, :default => 0.0
      t.float :net_payable, :default => 0.0
      t.float :net_amount, :default => 0.0
      t.boolean :is_active, :default => true
      t.integer :amc_id

      t.timestamps
    end
  end
end
