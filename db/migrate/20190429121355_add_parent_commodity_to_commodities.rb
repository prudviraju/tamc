class AddParentCommodityToCommodities < ActiveRecord::Migration
  def change
    add_column :commodities, :parent_category_id, :integer
  end
end
