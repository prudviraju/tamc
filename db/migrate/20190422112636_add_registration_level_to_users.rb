class AddRegistrationLevelToUsers < ActiveRecord::Migration
  def change
    add_column :users, :registred_level, :string
    add_column :users, :agent_type, :string
    add_column :users, :state, :string
    add_column :users, :locality, :string
    add_column :users, :gender, :string
    add_column :users, :apmc_licence_no, :string
    add_column :users, :pan_card_no, :string
    add_column :users, :company_name, :string
    add_column :users, :company_reg_no, :string
    add_column :users, :bank_name, :string
    add_column :users, :account_number, :string
    add_column :users, :ifsc_code, :string
    add_column :users, :trade_limit, :float, :default => 0.0
    add_column :users, :remarks, :text
    add_column :users, :profile_pic, :string
    add_column :users, :category, :string
    add_column :users, :service_provider, :string
  end
end
