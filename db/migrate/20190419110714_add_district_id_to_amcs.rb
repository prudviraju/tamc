class AddDistrictIdToAmcs < ActiveRecord::Migration
  def change
    add_column :amcs, :district_id, :integer
  end
end
