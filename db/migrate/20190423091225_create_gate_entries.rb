class CreateGateEntries < ActiveRecord::Migration
  def change
    create_table :gate_entries do |t|
      t.integer :amc_id
      t.string :gatepass_no
      t.string :entry_time
      t.string :vechile_no
      t.string :vechile_no
      t.integer :vehicle_type_id
      t.timestamps
    end
  end
end
