# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20190507092130) do

  create_table "amcs", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "is_active",            :default => true
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "state_id"
    t.integer  "district_id"
    t.float    "market_fee",           :default => 0.0
    t.float    "commission_agent_fee", :default => 0.0
  end

  create_table "bag_types", :force => true do |t|
    t.integer  "amc_id"
    t.decimal  "quandity",    :precision => 10, :scale => 0
    t.integer  "uom_id"
    t.string   "description"
    t.boolean  "is_active",                                  :default => true
    t.datetime "created_at",                                                   :null => false
    t.datetime "updated_at",                                                   :null => false
  end

  create_table "bag_weights", :force => true do |t|
    t.integer  "lot_id"
    t.integer  "weighment_id"
    t.integer  "bagTypeId"
    t.integer  "sequence",     :default => 0
    t.float    "netWeight",    :default => 0.0
    t.float    "grossWeight",  :default => 0.0
    t.float    "tareWeight",   :default => 0.0
    t.float    "price",        :default => 0.0
    t.string   "grade"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "sublot_id"
  end

  create_table "bids", :force => true do |t|
    t.integer  "lot_id"
    t.integer  "agent_id"
    t.integer  "seller_id"
    t.float    "bid_amount",  :default => 0.0
    t.boolean  "is_active",   :default => true
    t.boolean  "is_approved", :default => false
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "commodities", :force => true do |t|
    t.string   "name"
    t.integer  "uom_id"
    t.integer  "amc_id"
    t.boolean  "is_active",          :default => true
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.integer  "bag_type_id"
    t.integer  "parent_category_id"
    t.float    "bag_cost",           :default => 0.0
  end

  create_table "districts", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.integer  "state_id"
    t.boolean  "is_active",  :default => true
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "farmers", :force => true do |t|
    t.string   "name"
    t.string   "gender"
    t.date     "date_of_birth"
    t.date     "registration_date"
    t.string   "registration_level"
    t.string   "service_provider"
    t.string   "category"
    t.string   "locality"
    t.string   "village"
    t.string   "mandal"
    t.string   "district"
    t.string   "state"
    t.string   "pin_code"
    t.string   "contact_no"
    t.string   "email"
    t.string   "id_Proff_type"
    t.string   "proof_id"
    t.text     "remarks"
    t.string   "relation"
    t.string   "relative_name"
    t.string   "bank_name"
    t.string   "account_number"
    t.string   "ifsc_code"
    t.boolean  "is_active",          :default => true
    t.integer  "amc_id"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  create_table "fee_categories", :force => true do |t|
    t.string   "category_name"
    t.integer  "amc_id"
    t.boolean  "is_active",     :default => true
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "commodity_id"
  end

  create_table "fee_components", :force => true do |t|
    t.string   "name"
    t.string   "fee_type"
    t.float    "percentage",       :default => 0.0
    t.float    "amount",           :default => 0.0
    t.string   "party_type"
    t.float    "calculate_on_uom"
    t.float    "amount_per_uom",   :default => 0.0
    t.float    "tds",              :default => 0.0
    t.integer  "modified_by"
    t.datetime "modified_on"
    t.integer  "amc_id"
    t.integer  "fee_category_id"
    t.boolean  "is_active",        :default => true
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  create_table "gate_entries", :force => true do |t|
    t.integer  "amc_id"
    t.string   "gatepass_no"
    t.string   "entry_time"
    t.string   "vechile_no"
    t.integer  "vehicle_type_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.integer  "created_by"
    t.string   "device_id"
    t.string   "deviceType"
    t.string   "gate_entry_type"
    t.string   "tempLot",         :default => "N"
    t.string   "trade_type",      :default => "N"
    t.date     "posted_on"
  end

  create_table "gunny_details", :force => true do |t|
    t.integer  "commodity_id"
    t.integer  "bag_type_id"
    t.float    "tare_weight",  :default => 0.0
    t.integer  "uom_id"
    t.boolean  "is_active",    :default => true
    t.integer  "amc_id"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "lot_deductions", :force => true do |t|
    t.integer  "bagSeqno"
    t.string   "lot_code"
    t.integer  "lot_id"
    t.float    "gross_Weight",     :default => 0.0
    t.float    "tare_weight",      :default => 0.0
    t.float    "net_weight",       :default => 0.0
    t.float    "bagamount",        :default => 0.0
    t.float    "deduction_amount", :default => 0.0
    t.string   "party_type"
    t.string   "charge_type"
    t.float    "charge_value",     :default => 0.0
    t.string   "deduction_type"
    t.boolean  "is_active",        :default => true
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  create_table "lots", :force => true do |t|
    t.string   "seller_type"
    t.integer  "seller_id"
    t.string   "mobile_no"
    t.string   "state"
    t.string   "district"
    t.string   "mandal"
    t.string   "village"
    t.string   "commodity_id"
    t.integer  "bag_type_id"
    t.integer  "no_of_bags",            :default => 0
    t.float    "aprox_quantity",        :default => 0.0
    t.integer  "vehicle_type_id"
    t.string   "vechile_no"
    t.string   "ca_firm"
    t.integer  "ca_name"
    t.integer  "amc_id"
    t.string   "gate_entry_pass_no"
    t.string   "lot_code"
    t.boolean  "is_gate_entry",         :default => false
    t.boolean  "is_bid_rigistred",      :default => false
    t.boolean  "is_weighment",          :default => false
    t.boolean  "is_takpatti",           :default => false
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.float    "bid_amount",            :default => 0.0
    t.integer  "bid_raise_by"
    t.date     "posted_on"
    t.float    "netWeight",             :default => 0.0
    t.float    "grossWeight",           :default => 0.0
    t.float    "tareWeight",            :default => 0.0
    t.float    "gross_amount",          :default => 0.0
    t.float    "net_payable",           :default => 0.0
    t.float    "net_amount",            :default => 0.0
    t.float    "rate_for_qui",          :default => 0.0
    t.integer  "finalised_bag_count"
    t.string   "transaction_no"
    t.string   "invoice_doc_number"
    t.float    "farmer_amount",         :default => 0.0
    t.float    "trader_payable_amount", :default => 0.0
    t.float    "market_charges",        :default => 0.0
    t.float    "ca_charges",            :default => 0.0
    t.float    "gunny_amount",          :default => 0.0
  end

  create_table "mandals", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.integer  "state_id"
    t.integer  "district_id"
    t.boolean  "is_active",   :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "market_charges", :force => true do |t|
    t.string   "name"
    t.string   "unit"
    t.float    "above_weight",   :default => 0.0
    t.float    "below_weight",   :default => 0.0
    t.float    "charge_value",   :default => 0.0
    t.string   "charge_type"
    t.string   "deduction_type"
    t.integer  "commodity_id"
    t.integer  "amc_id"
    t.string   "party_type"
    t.integer  "modified_by"
    t.date     "modified_on"
    t.string   "attachement"
    t.boolean  "is_active",      :default => true
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "sale_bill_components", :force => true do |t|
    t.integer  "component_id"
    t.float    "amount",                 :default => 0.0
    t.float    "deducted_amount",        :default => 0.0
    t.string   "amount_calculated_type", :default => "0.0"
    t.float    "percent",                :default => 0.0
    t.float    "discount_percent",       :default => 0.0
    t.string   "party_type"
    t.string   "party_name"
    t.integer  "party_id"
    t.integer  "salebill_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  create_table "salebills", :force => true do |t|
    t.string   "agreement_type"
    t.string   "seller_type"
    t.integer  "seller_id"
    t.integer  "trader_id"
    t.string   "firm_name"
    t.integer  "lot_id"
    t.string   "lot_code"
    t.integer  "commodity_id"
    t.float    "apx_weight",      :default => 0.0
    t.float    "rate_for_qui",    :default => 0.0
    t.float    "net_weight",      :default => 0.0
    t.float    "actual_weight",   :default => 0.0
    t.integer  "fee_category_id"
    t.integer  "bag_type_id"
    t.integer  "no_of_bags",      :default => 0
    t.float    "net_amount",      :default => 0.0
    t.float    "gross_amount",    :default => 0.0
    t.float    "net_payable",     :default => 0.0
    t.integer  "amc_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "segments", :force => true do |t|
    t.integer  "amc_id"
    t.string   "segment_name"
    t.boolean  "is_active",    :default => true
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  create_table "states", :force => true do |t|
    t.string   "name"
    t.boolean  "is_active",  :default => true
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "sub_lots", :force => true do |t|
    t.integer  "lot_id"
    t.float    "bid_amount",                  :default => 0.0
    t.integer  "no_of_bags"
    t.float    "grossWeight",                 :default => 0.0
    t.float    "netWeight",                   :default => 0.0
    t.float    "tareWeight",                  :default => 0.0
    t.integer  "user_id"
    t.string   "device_id"
    t.float    "gross_amount",                :default => 0.0
    t.float    "net_payable",                 :default => 0.0
    t.float    "net_amount",                  :default => 0.0
    t.boolean  "is_active",                   :default => true
    t.integer  "amc_id"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.string   "sub_lot_id",   :limit => 100,                   :null => false
  end

  create_table "unit_of_measurements", :force => true do |t|
    t.string   "unit_name"
    t.text     "description"
    t.integer  "amc_id"
    t.boolean  "is_active",   :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "user_roles", :force => true do |t|
    t.string   "role_name"
    t.boolean  "is_active",  :default => true
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "user_type"
    t.string   "user_code"
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "mobile_no"
    t.string   "aadhar_no"
    t.string   "door_no"
    t.string   "village"
    t.string   "mandal"
    t.string   "district"
    t.string   "pincode"
    t.string   "email"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.string   "perishable_token"
    t.string   "password"
    t.string   "password_confirmation"
    t.boolean  "is_admin",              :default => false
    t.boolean  "is_super_admin",        :default => false
    t.string   "user_role"
    t.integer  "amc_id"
    t.string   "password1"
    t.boolean  "is_active",             :default => true
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "registred_level"
    t.string   "agent_type"
    t.string   "state"
    t.string   "locality"
    t.string   "gender"
    t.string   "apmc_licence_no"
    t.string   "pan_card_no"
    t.string   "company_name"
    t.string   "company_reg_no"
    t.string   "bank_name"
    t.string   "account_number"
    t.string   "ifsc_code"
    t.float    "trade_limit",           :default => 0.0
    t.text     "remarks"
    t.string   "profile_pic"
    t.string   "category"
    t.string   "service_provider"
    t.boolean  "is_employee",           :default => false
    t.string   "user_code"
    t.integer  "segment_id"
  end

  create_table "vehicle_types", :force => true do |t|
    t.string   "name"
    t.integer  "amc_id"
    t.text     "description"
    t.boolean  "is_active",   :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "villages", :force => true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.integer  "district_id"
    t.integer  "mandal_id"
    t.boolean  "is_active",   :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "weighments", :force => true do |t|
    t.string   "weighment_type"
    t.string   "lot_code"
    t.integer  "lot_id"
    t.string   "seller_type"
    t.integer  "bag_type"
    t.integer  "product_id"
    t.integer  "seller_name"
    t.integer  "no_of_bags"
    t.float    "lot_weight",        :default => 0.0
    t.float    "lot_weight_in_no",  :default => 0.0
    t.float    "weighment_charges", :default => 0.0
    t.text     "remarks"
    t.integer  "amc_id"
    t.boolean  "is_active",         :default => true
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

end
