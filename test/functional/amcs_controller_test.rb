require 'test_helper'

class AmcsControllerTest < ActionController::TestCase
  setup do
    @amc = amcs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:amcs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create amc" do
    assert_difference('Amc.count') do
      post :create, amc: { code: @amc.code, is_active: @amc.is_active, name: @amc.name }
    end

    assert_redirected_to amc_path(assigns(:amc))
  end

  test "should show amc" do
    get :show, id: @amc
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @amc
    assert_response :success
  end

  test "should update amc" do
    put :update, id: @amc, amc: { code: @amc.code, is_active: @amc.is_active, name: @amc.name }
    assert_redirected_to amc_path(assigns(:amc))
  end

  test "should destroy amc" do
    assert_difference('Amc.count', -1) do
      delete :destroy, id: @amc
    end

    assert_redirected_to amcs_path
  end
end
